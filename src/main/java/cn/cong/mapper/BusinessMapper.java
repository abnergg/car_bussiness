package cn.cong.mapper;

import cn.cong.domain.Business;
import cn.cong.domain.Department;
import cn.cong.qo.BusinessQueryObject;
import cn.cong.qo.QueryObject;

import java.util.List;

public interface BusinessMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Business record);

    Business selectByPrimaryKey(Long id);

    List<Business> selectAll();

    int updateByPrimaryKey(Business record);

    List<Business> selectForList(BusinessQueryObject qo);

    Business selectBusinessByMainStoreId(int mainStoreId);
}