package cn.cong.mapper;

import cn.cong.domain.Permission;
import cn.cong.domain.Role;
import cn.cong.qo.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Role role);

    Role selectByPrimaryKey(Long id);

    List<Role> selectAll();

    int updateByPrimaryKey(Role role);

    List<Role> selectForList(QueryObject qo);

    void insertRelation(@Param("id") Long id, @Param("permissionId") Long permissionId);

    List<Permission> selectByRoleId(Long id);

    void deleteRolePermission(Long roleId);
}