package cn.cong.mapper;

import cn.cong.domain.Employee;
import cn.cong.domain.Role;
import cn.cong.qo.EmployeeQueryObject;
import cn.cong.qo.PageResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeMapper {
    int deleteByPrimaryKey(Long id);
    int insert(Employee employee);
    Employee selectByPrimaryKey(Long id);
    List<Employee> selectAll();
    int updateByPrimaryKey(Employee employee);
    List<Employee> selectForList(EmployeeQueryObject eqo);

    void insertRelation(@Param("id") Long id, @Param("roleId") Long roleId);

    List<Role> selectRolesByRoleId(@Param("employeeId") Long employeeId);

    void deleteByEmployeeId(@Param("employeeId") Long employeeId);

    Employee checkUsername(String username);

    Employee login(@Param("username") String username, @Param("password") String password);
}