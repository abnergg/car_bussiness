package cn.cong.mapper;

import cn.cong.domain.Menu;
import cn.cong.qo.MenuQueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MenuMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Menu record);

    Menu selectByPrimaryKey(Long id);

    List<Menu> selectAll();

    int updateByPrimaryKey(Menu record);

    List<Menu> selectForList(MenuQueryObject qo);

    List<Menu> selectParent();

    void deletePermissionMenuByMenuId(Long id);

    void insertByPermissionId(@Param("permissionId") Long permissionId, @Param("id") Long id);

    void updateStatusByMenuId(@Param("menuId") Long id, @Param("status") boolean status);
}