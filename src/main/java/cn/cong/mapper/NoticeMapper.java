package cn.cong.mapper;

import cn.cong.domain.Notice;
import cn.cong.qo.NoticeQueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface NoticeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Notice record);

    Notice selectByPrimaryKey(@Param("id") Long id);

    List<Notice> selectAll();

    int updateByPrimaryKey(Notice record);

    List<Notice> selectForList (NoticeQueryObject qo);

    void updateStatus(@Param("noticeId") Long noticeId, @Param("status") Integer status);

    void updateReadStatus(Long noticeId);

    List<Map<String, Object>> selectStatusByEmployeeNotice(@Param("noticeId") Long noticeId, @Param("employeeId") Long employeeId);

    void insertEmployeeNotice(@Param("id") Long id, @Param("employeeId") Long employeeId);
}