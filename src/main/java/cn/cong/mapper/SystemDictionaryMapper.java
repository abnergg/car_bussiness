package cn.cong.mapper;

import cn.cong.domain.SystemDictionary;
import cn.cong.qo.QueryObject;
import cn.cong.qo.SystemDictionaryQueryObject;

import java.util.List;

public interface SystemDictionaryMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SystemDictionary record);

    SystemDictionary selectByPrimaryKey(Long id);

    List<SystemDictionary> selectAll();

    int updateByPrimaryKey(SystemDictionary record);

    List<SystemDictionary> selectForList(SystemDictionaryQueryObject qo);

    List<SystemDictionary> selectBusinessNameByParentId(long parentId);
}