package cn.cong.mapper;

import cn.cong.domain.Department;
import cn.cong.domain.MessageBoard;
import cn.cong.qo.MessageBoardQueryObject;
import cn.cong.qo.QueryObject;

import java.util.List;
import java.util.Map;

public interface MessageBoardMapper {
    int deleteByPrimaryKey(Long id);

    int insert(MessageBoard record);

    MessageBoard selectByPrimaryKey(Long id);

    List<MessageBoard> selectAll();

    int updateByPrimaryKey(MessageBoard record);

    List<MessageBoard> selectForList(MessageBoardQueryObject qo);

    List<Map<String, Object>> selectMessageReplyByBoardId(Long id);
}