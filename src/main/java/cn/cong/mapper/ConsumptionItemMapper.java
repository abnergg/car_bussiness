package cn.cong.mapper;

import cn.cong.domain.ConsumptionItem;
import java.util.List;

public interface ConsumptionItemMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ConsumptionItem record);

    ConsumptionItem selectByPrimaryKey(Long id);

    List<ConsumptionItem> selectAll();

    int updateByPrimaryKey(ConsumptionItem record);

    List<ConsumptionItem> selectByCno(String cno);
}