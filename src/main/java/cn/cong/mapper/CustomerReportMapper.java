package cn.cong.mapper;

import cn.cong.domain.Appointment;
import cn.cong.qo.AppointmentQueryObject;
import cn.cong.qo.CustomerReportQueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CustomerReportMapper {

    List<Map<String,Object>> selectForList(CustomerReportQueryObject qo);

    List<Map<String, Object>> selectForListByGroupType(@Param("groupType") String groupType);
}