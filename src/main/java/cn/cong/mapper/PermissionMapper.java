package cn.cong.mapper;

import cn.cong.domain.Permission;
import cn.cong.qo.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface PermissionMapper {
    int insert(Permission permission);

    int deleteByPrimaryKey(Long id);

    int updateByPrimaryKey(Permission permission);

    Permission selectByPrimaryKey(Long id);

    List<Permission> selectAll();

    List<Permission> selectForList(QueryObject qo);

    //批量插入
    void batchInsert(@Param("permissions") Set<Permission> permissions);

    List<String> selectExpressionByEmployeeId(Long employeeId);

    List<Permission> selectByMenuId(Long id);
}