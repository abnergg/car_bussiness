package cn.cong.mapper;

import cn.cong.domain.Appointment;
import cn.cong.domain.SystemDictionary;
import cn.cong.qo.AppointmentQueryObject;
import cn.cong.qo.SystemDictionaryQueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AppointmentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Appointment record);

    Appointment selectByPrimaryKey(Long id);

    List<Appointment> selectAll();

    int updateByPrimaryKey(Appointment record);

    List<Appointment> selectForList(AppointmentQueryObject qo);

    void updateStatusById(@Param("status") int status, @Param("id") Long id);
}