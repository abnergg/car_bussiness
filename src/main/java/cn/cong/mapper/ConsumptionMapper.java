package cn.cong.mapper;

import cn.cong.domain.Consumption;
import cn.cong.domain.Department;
import cn.cong.qo.ConsumptionQueryObject;
import cn.cong.qo.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ConsumptionMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Consumption record);

    Consumption selectByPrimaryKey(Long id);

    List<Consumption> selectAll();

    int updateByPrimaryKey(Consumption record);

    List<Consumption> selectForList(ConsumptionQueryObject qo);

    void updateStatusConsumeIdById(Consumption consumption);

    void updateStatusAuditIdById(Consumption consumption);
}