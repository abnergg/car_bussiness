package cn.cong.qo;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class ConsumptionItemQueryObject extends QueryObject {
    private String con;
    private Long categoryId;
    private Long payTypeId;
    private BigDecimal amount;
    private BigDecimal discountAmount;
    private BigDecimal payAmount;
}
