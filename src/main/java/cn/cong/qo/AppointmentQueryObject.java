package cn.cong.qo;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter
@Setter
public class AppointmentQueryObject extends QueryObject {
    private String ano;
    private int status = -1;
    private int businessId = -1;
    private String contactName;
    private String contactTel;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
}
