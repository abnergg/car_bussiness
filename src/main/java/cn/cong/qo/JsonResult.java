package cn.cong.qo;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class JsonResult{
    private Boolean success;
    private String msg;

    public JsonResult(Boolean success, String msg) {
        this.success = success;
        this.msg = msg;
    }
}
