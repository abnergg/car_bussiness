package cn.cong.qo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MenuQueryObject extends QueryObject {
    private String name;
    private String url;
    private Integer status;
    private Long parentId;
}
