package cn.cong.qo;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author Abner
 * @Date 2021/5/9 14:14
 * @Version 1.0
 */

@Getter
@Setter
public class MessageBoardQueryObject extends QueryObject{

}
