package cn.cong.qo;

import lombok.Data;

@Data
public class SystemDictionaryQueryObject extends QueryObject {
    private Long parentId;
    private Long nodeId;
}
