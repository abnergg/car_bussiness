package cn.cong.qo;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter
@Setter
public class ConsumptionQueryObject extends QueryObject {
    private Integer status;
    private Long businessId;
    private String customerName;
    private String customerTel;
    private String appointmentAno;
    private String cno;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date starDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
}
