package cn.cong.domain;

import com.alibaba.fastjson.JSON;
import lombok.Data;

@Data
public class Department {
    private Long id;
    private String name;
    private String sn;

    public String toJSON() {
        //{"id":+id+,"name":+name+,"sn":+sn+}
//        return "{id:" + id + ",name:" + name + ",sn:" + sn + "}";
        return JSON.toJSONString(this);
    }
}