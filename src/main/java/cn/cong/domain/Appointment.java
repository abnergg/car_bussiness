package cn.cong.domain;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class Appointment {

    public static final int STATUS_TYPE_APPOINT = 63;  //待确认
    public static final int STATUS_TYPE_PERFORM = 64;  //履行中
    public static final int STATUS_TYPE_CONSUME = 65;  //消费中
    public static final int STATUS_TYPE_PLACEON = 66;  //归档中
    public static final int STATUS_TYPE_ABANDON = 67;  //废弃

    /** 主键*/
    private Long id;

    /** 预约单流水号*/
    private String ano;

    /** 预约单状态 （预约中/履行中/消费中/归档/废弃单）*/
    private int status = 0;

    /** 业务大类*/
//    private Long categoryId;
    private SystemDictionary systemDictionary;

    /** 预约说明*/
    private String info;

    /** 联系电话*/
    private String contactTel;

    /** 联系人名称*/
    private String contactName;

    /** 预约门店*/
//    private Long businessId;
    private Business business;

    /** 创建时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    /** 预约时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date appointmentTime;

    public String statusDisplay(){
        switch (status){
            case STATUS_TYPE_APPOINT:return "待确认";
            case STATUS_TYPE_PERFORM:return "履行中";
            case STATUS_TYPE_CONSUME:return "消费中";
            case STATUS_TYPE_PLACEON:return "归档中";
            case STATUS_TYPE_ABANDON:return "废弃";
            default:return "异常";
        }
    }

    public String toJSON(){
        Map<String,Object> map = new HashMap<>();
        map.put("id",id);
        Long id = business.getId();
        System.out.println(id);
        map.put("businessId", id);
        map.put("appointmentTime", new SimpleDateFormat("yyyy-MM-dd").format(appointmentTime));
        map.put("parentId",systemDictionary.getParentId());
        map.put("categoryId",systemDictionary.getId());
        map.put("contactName",contactName);
        map.put("contactTel",contactTel);
        map.put("info",info);
        return JSON.toJSONString(map);
    }
}