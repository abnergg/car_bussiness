package cn.cong.domain;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class Notice {

    private Long id;

    private String info;

    private String title;

    private Long creatId;

    private Date creatTime;

    private Integer level;

    private Boolean readStatus;

    private Boolean status;

    private String creatName;

    public String levelDisplay() {
        if (level != null) {
            switch (level) {
                case 0:
                    return "紧要";
                case 1:
                    return "重要";
                case 2:
                    return "普通";
                default:
                    return "异常";
            }
        }
        return "";
    }

    public String toJson() {
        Map<String, Object> map = new HashMap<>();
        map.put("id",id);
        map.put("info",info);
        map.put("title",title);
        map.put("level",level);
        return JSON.toJSONString(map);
    }
}