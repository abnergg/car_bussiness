package cn.cong.domain;

import com.alibaba.fastjson.JSON;
import lombok.Data;

import java.util.List;

@Data
public class SystemDictionary {
    private Long id;

    private String name;

    private String sn;

    private String intro;

    private Long parentId;

    private String parentName;

    private List<SystemDictionary> children;

    public String toJSON() {
        return JSON.toJSONString(this);
    }
}