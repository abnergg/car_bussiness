package cn.cong.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter
@Setter
public class MessageBoard {
    /** */
    private Long id;

    /** 昵称*/
    private String nickname;

    /** 留言内容*/
    private String content;

    /** 留言时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    /** 预约业务*/
    private SystemDictionary systemDictionary;

    /** 回复状态(未回复/已回复)*/
    private Boolean replystatus;

    private int count;

}