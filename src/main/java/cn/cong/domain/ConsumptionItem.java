package cn.cong.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class ConsumptionItem {
    /** 主键*/
    private Long id;

    /** 业务小类*/
    private SystemDictionary category;

    /** 结算类型*/
    private Long payTypeId;

    /** 应收金额*/
    private BigDecimal amount;

    /** 实收金额*/
    private BigDecimal payAmount;

    /** 优惠金额*/
    private BigDecimal discountAmount;

    /** 创建人*/
    private Long createUserId;

    /** 创建时间*/
    private Date createTime;

    /** 结算单流水号*/
    private String cno;

    private String payTypeName;
}