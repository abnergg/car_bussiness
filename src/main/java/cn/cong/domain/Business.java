package cn.cong.domain;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class Business {

    public static final int MAINSTORE_MAINSTORE = 1;
    public static final int MAINSTORE_BRANCHSTORE = 0;


    /** 主键*/
    private Long id;
    /** 门店名称*/
    private String name;
    /** 门店介绍*/
    private String intro;
    /** 经营范围*/
    private String scope;
    /** 门店电话*/
    private String tel;
    /** 门店地址*/
    private String address;
    /** 经营日期*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date openDate;
    /** 营业执照图片*/
    private String licenseImg;
    /** 营业执照号码*/
    private String licenseNumber;
    /** 法人姓名*/
    private String legalName;
    /** 法人电话*/
    private String legalTel;
    /** 法人身份证*/
    private String legalIdcard;
    /** 门店性质(总店/分店)*/
    private int mainStore = MAINSTORE_BRANCHSTORE;

    public String toJson(){
        Map<String,Object> map = new HashMap<>();
        map.put("name",name);
        map.put("mainStore",mainStore);
        map.put("tel",tel);
        map.put("address",address);
        map.put("openDate",new SimpleDateFormat("yyyy-MM-dd").format(openDate));
        map.put("intro",intro);
        map.put("scope",scope);
        map.put("licenseNumber",licenseNumber);
        map.put("legalName",legalName);
        map.put("legalTel",legalTel);
        map.put("licenseImg",licenseImg);
        return JSON.toJSONString(map);
    }
}