package cn.cong.domain;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class Menu {
    /** */
    private Long id;

    /** 中文名称*/
    private String name;

    /** 菜单英文名称*/
    private String sn;

    /** 菜单地址*/
    private String url;

    /** 图标*/
    private String icon;

    /** 父类id*/
    private Long parentId;

    /** 父类名称*/
    private String parentName;

    /** 显示顺序*/
    private Integer sort;

    /** 创建时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    /** 状态(0,1)(不显示，显示)*/
    private Boolean status;

    private List<Permission> permissions;

    private List<Menu> children;

    public String menuTypeDisplay(){
        if (parentId!=null){
            return "菜单";
        }
        else {
            return "目录";
        }
    }

    public String toJson(){
        Map<String,Object> map = new HashMap<>();
        map.put("id",id);
        map.put("sn",sn);
        map.put("status",status);
        map.put("createTime",new SimpleDateFormat("yyyy-MM-dd").format(createTime));
        map.put("parentName",parentName);
        map.put("parentId",parentId);
        map.put("name",name);
        map.put("url",url);
        map.put("sort",sort);
        map.put("icon",icon);
        map.put("permissions",permissions);
        return JSON.toJSONString(map);
    }


}