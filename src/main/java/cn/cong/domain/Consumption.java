package cn.cong.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class Consumption {

    public static final int STATUS_TYPE_CONSUME = 68;  //待结算
    public static final int STATUS_TYPE_AUDIT = 69;  //待审核
    public static final int STATUS_TYPE_FINISH = 70;  //归档
    public static final int STATUS_TYPE_GAILURE = 71;  //坏账
    /** 主键*/
    private Long id;

    /** 消费单流水号*/
    private String cno;

    /** 消费单状态 （待结算/待审核/归档/坏账）*/
    private Integer status;

    /** 总消费金额*/
    private BigDecimal totalAmount;

    /** 实收金额*/
    private BigDecimal payAmount;

    /** 优惠金额*/
    private BigDecimal discountAmount;

    /** 结算备注*/
    private String info;

    /** 结算时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date payTime;

    /** 结算人*/
//    private Long payeeId;
    private Employee employee;

    /** 客户名称*/
    private String customerName;

    /** 客户联系方式*/
    private String customerTel;

    /** 车牌信息记录*/
    private String carLicence;

    /** 车型记录*/
    private String carType;

    /** 关联预约单*/
    private String appointmentAno;

    /** 进店时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date checkinTime;

    /** 离店时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date checkoutTime;

    /** 消费门店*/
//    private Long businessId;
    private Business business;

    /** 创建时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    /** 创建人*/
    private Long createUserId;

    /** 审核时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date auditTime;

    /** 审核人*/
    private Long auditorId;
    private String auditorName;

    public String statusDisplay(){
        switch (status){
            case STATUS_TYPE_CONSUME:return "待结算";
            case STATUS_TYPE_AUDIT:return "待审核";
            case STATUS_TYPE_FINISH:return "归档";
            case STATUS_TYPE_GAILURE:return "坏账";
            default:return "异常";
        }
    }
}