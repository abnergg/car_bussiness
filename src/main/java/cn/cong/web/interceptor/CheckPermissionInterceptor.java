package cn.cong.web.interceptor;

import cn.cong.domain.Employee;
import cn.cong.domain.Permission;
import cn.cong.service.IPermissionService;
import cn.cong.util.RequiredPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class CheckPermissionInterceptor implements HandlerInterceptor {

    @Autowired
    private IPermissionService permissionService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Employee employee = (Employee) request.getSession().getAttribute("EMPLOYEE_IN_SESSION");
        if (employee.isAdmin()) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        RequiredPermission annotation = handlerMethod.getMethodAnnotation(RequiredPermission.class);
        if (annotation==null) {
            return true;
        }
        String expression = annotation.expression();
        List<String> expressions = (List<String>) request.getSession().getAttribute("EXPRESSIONS_IN_SESSION");
        if (expressions.contains(expression)) {
            return true;
        }
        // 不是管理员， 方法需要权限控制，但此员工有没有权限
        response.sendRedirect("/nopermission");
        return false;
    }
}
