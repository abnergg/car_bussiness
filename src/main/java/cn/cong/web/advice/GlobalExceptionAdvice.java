package cn.cong.web.advice;

import cn.cong.domain.Menu;
import cn.cong.qo.JsonResult;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class GlobalExceptionAdvice {

    @Autowired
    private ApplicationContext ctx;

    // 处理异常的方法，方法需要贴 ExceptionHandler 注解
    @ExceptionHandler(RuntimeException.class)
    public String handlerException(RuntimeException e, HandlerMethod method,
                                   Model model, HttpServletResponse response) {
        e.printStackTrace();

        if (method.hasMethodAnnotation(ResponseBody.class)) {
            try {
                response.setContentType("application/json;charset=UTF-8");
                JsonResult jsonResult = new JsonResult(false, e.getMessage() + "系统异常，请联系管理员");
                response.getWriter().print(JSON.toJSONString(jsonResult));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            return null;
        }
        model.addAttribute("errorMsg", e.getMessage());
        return "common/error";
    }
}
