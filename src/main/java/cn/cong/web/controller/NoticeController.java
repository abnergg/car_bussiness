package cn.cong.web.controller;

import cn.cong.domain.Notice;
import cn.cong.qo.NoticeQueryObject;
import cn.cong.service.INoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/notice")
public class NoticeController {

    @Autowired
    private INoticeService noticeService;

    @RequestMapping("/list")
    public String list(@ModelAttribute("qo") NoticeQueryObject qo, Model model) {
        model.addAttribute("pageInfo", noticeService.query(qo));
        return "/notice/list";
    }

    @RequestMapping("/checked")
    public String checked(Long id, Model model) {
        Notice notice = noticeService.selectById(id);
        model.addAttribute("notice",notice);
        noticeService.updateReadStatus(id);
        noticeService.insertEmployeeNotice(id);
        return "/notice/show";
    }

    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(Notice notice) {
        if (notice.getId() != null) {
            noticeService.update(notice);
        }else {
            noticeService.insert(notice);
        }
        return "redirect:/notice/list";
    }

    @RequestMapping("/updateStatus")
    public String updateStatus(Long noticeId,Integer status) {
        noticeService.updateStatus(noticeId,status);
        return "redirect:/notice/list";
    }
}
