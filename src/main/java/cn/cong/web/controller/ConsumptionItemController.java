package cn.cong.web.controller;

import cn.cong.domain.ConsumptionItem;
import cn.cong.qo.ConsumptionItemQueryObject;
import cn.cong.qo.JsonResult;
import cn.cong.qo.QueryObject;
import cn.cong.service.IConsumptionItemService;
import cn.cong.util.RequiredPermission;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/consumptionItem")
public class ConsumptionItemController {

    @Autowired
    private IConsumptionItemService consumptionItemService;

    @RequestMapping("/saveOrUpdate")
    @RequiredPermission(name="结算单明细编辑", expression="consumptionItem:saveOrUpdate")
    @ResponseBody
    public JsonResult saveOrUpdate(ConsumptionItem consumptionItem){

        consumptionItemService.insert(consumptionItem);

        return new JsonResult(true,"添加成功");
    }

    @RequestMapping("/delete")
    @RequiredPermission(name="结算单明细删除", expression="consumptionItem:delete")
    @ResponseBody
    public JsonResult delete(Long[] ids){
        consumptionItemService.delete(ids);
        return new JsonResult(true,"删除成功");
    }
}
