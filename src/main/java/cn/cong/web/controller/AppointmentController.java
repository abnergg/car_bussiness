package cn.cong.web.controller;

import cn.cong.domain.Appointment;
import cn.cong.domain.Business;
import cn.cong.domain.SystemDictionary;
import cn.cong.qo.AppointmentQueryObject;
import cn.cong.qo.QueryObject;
import cn.cong.service.IAppointmentService;
import cn.cong.service.IBusinessService;
import cn.cong.service.ISystemDictionaryService;
import cn.cong.util.RequiredPermission;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/appointment")
public class AppointmentController {

    @Autowired
    private IAppointmentService appointmentService;

    @Autowired
    private IBusinessService businessService;

    @Autowired
    private ISystemDictionaryService systemDictionaryService;


    @RequestMapping("/list")
    @RequiredPermission(name="预约查询", expression="appointment:list")

    public String list(@ModelAttribute("qo") AppointmentQueryObject qo, Model model){
        PageInfo<Appointment> query = appointmentService.query(qo);
        model.addAttribute("pageInfo",query);
        List<Business> businesses = businessService.selectAll();
        model.addAttribute("businesses",businesses);
        List<SystemDictionary> systemDictionaries1 = systemDictionaryService.queryBusinessNameByParentId(62);
        model.addAttribute("systemDictionaries1",systemDictionaries1);
        List<SystemDictionary> systemDictionaries = systemDictionaryService.queryBusinessNameByParentId(1);
        model.addAttribute("systemDictionaries",systemDictionaries);

        return "/appointment/list";
    }

    @RequestMapping("/input")
    @RequiredPermission(name="预约编辑", expression="appointment:saveOrUpdate")
    public String input(Long id,Model model){
        if (id!=null) {
            Appointment appointment = appointmentService.selectById(id);
            model.addAttribute("appointment",appointment);
        }
        return "appointment/input";
    }

    @RequestMapping("/saveOrUpdate")
    @RequiredPermission(name="预约编辑", expression="appointment:saveOrUpdate")
    public String saveOrUpdate(Appointment appointment){
        if (appointment.getId()!=null) {
            appointmentService.update(appointment);
        }else {
            appointmentService.insert(appointment);
        }
        return "redirect:/appointment/list";
    }

    @RequestMapping("/delete")
    @RequiredPermission(name="预约删除", expression="appointment:delete")
    public String delete(Long id){
        appointmentService.delete(id);
        return "redirect:/appointment/list";
    }

    @RequestMapping("/updateStatus")
    public String updateStatus(int status,Long id){
        appointmentService.updateStatusById(status,id);
        return "redirect:/appointment/list";
    }

    @RequestMapping("/queryByParentId")
    @ResponseBody
    public List<SystemDictionary> queryByParentId(Long parentId){

        return systemDictionaryService.queryBusinessNameByParentId(parentId);
    }
}
