package cn.cong.web.controller;

import cn.cong.domain.Employee;
import cn.cong.qo.JsonResult;
import cn.cong.service.IEmployeeService;
import cn.cong.service.IPermissionService;
import cn.cong.util.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class LoginController {

    @Autowired
    private IEmployeeService employeeService;

    @Autowired
    private IPermissionService permissionService;

    @RequestMapping("/login")
    @ResponseBody
    public JsonResult login(String username, String password, HttpSession session) {
        Employee employee = employeeService.login(username, password);
        if (employee != null) {
            UserContext.setEmployee(employee);

            if (!employee.isAdmin()) {
                List<String> expressions = permissionService.queryExpressionByEmployeeId(employee.getId());
                UserContext.setExpressions(expressions);
            }

            return new JsonResult(true, "登录成功");
        } else {
            return new JsonResult(false, "登录失败");
        }
    }

    @RequestMapping("/logout")
    public String logout(HttpSession session) {
        session.invalidate();
        return "redirect:/static/login.html";
    }

    @RequestMapping("/nopermission")
    public String nopermission(){
        return "/common/nopermission";
    }
}
