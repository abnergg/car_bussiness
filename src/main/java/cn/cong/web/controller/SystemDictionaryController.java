package cn.cong.web.controller;

import cn.cong.domain.SystemDictionary;
import cn.cong.qo.JsonResult;
import cn.cong.qo.QueryObject;
import cn.cong.qo.SystemDictionaryQueryObject;
import cn.cong.service.ISystemDictionaryService;
import cn.cong.util.RequiredPermission;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/systemDictionary")
public class SystemDictionaryController {

    @Autowired
    private ISystemDictionaryService systemDictionaryService;

    @RequestMapping("/list")
    public String list(@ModelAttribute("qo") SystemDictionaryQueryObject qo, Model model){
        PageInfo<SystemDictionary> query = systemDictionaryService.query(qo);
        model.addAttribute("pageInfo",query);
        return "/systemDictionary/list";
    }

    @RequestMapping("/treeData")
    @ResponseBody
    public List<Map> treeData(){
        List<Map> maps = systemDictionaryService.queryTreeData();
        return maps;
    }

    @RequestMapping("/input")
    public String input(Long id,Model model){
        if (id!=null) {
            SystemDictionary systemDictionary = systemDictionaryService.selectById(id);
            model.addAttribute("systemDictionary",systemDictionary);
        }
        return "systemDictionary/input";
    }

    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public JsonResult saveOrUpdate(SystemDictionary systemDictionary){
        if (systemDictionary.getId()!=null) {
            systemDictionaryService.update(systemDictionary);
            return new JsonResult(true,"更新成功");
        }else {
            systemDictionaryService.insert(systemDictionary);
            return new JsonResult(true,"插入成功");
        }
    }

    @RequestMapping("/delete")
    public String delete(Long id){
        systemDictionaryService.delete(id);
        return "redirect:/systemDictionary/list";
    }
}
