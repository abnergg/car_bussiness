package cn.cong.web.controller;

import cn.cong.domain.Department;
import cn.cong.qo.QueryObject;
import cn.cong.service.IDepartmentService;
import cn.cong.util.RequiredPermission;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private IDepartmentService departmentService;

    @RequestMapping("/list")
    @RequiredPermission(name="部门查询", expression="department:list")
    public String list(QueryObject qo,Model model){
        PageInfo<Department> query = departmentService.query(qo);
        model.addAttribute("pageInfo",query);
        return "/department/list";
    }

    @RequestMapping("/input")
    @RequiredPermission(name="部门编辑", expression="department:saveOrUpdate")
    public String input(Long id,Model model){
        if (id!=null) {
            Department department = departmentService.selectById(id);
            model.addAttribute("department",department);
        }
        return "department/input";
    }

    @RequestMapping("/saveOrUpdate")
    @RequiredPermission(name="部门编辑", expression="department:saveOrUpdate")
    public String saveOrUpdate(Department department){
        if (department.getId()!=null) {
            departmentService.update(department);
        }else {
            departmentService.insert(department);
        }
        return "redirect:/department/list";
    }

    @RequestMapping("/delete")
    @RequiredPermission(name="部门删除", expression="department:delete")
    public String delete(Long id){
        departmentService.delete(id);
        return "redirect:/department/list";
    }
}
