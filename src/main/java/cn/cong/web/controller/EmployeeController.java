package cn.cong.web.controller;

import cn.cong.domain.Department;
import cn.cong.domain.Employee;
import cn.cong.domain.Role;
import cn.cong.qo.EmployeeQueryObject;
import cn.cong.qo.JsonResult;
import cn.cong.service.IDepartmentService;
import cn.cong.service.IEmployeeService;
import cn.cong.service.IRoleService;
import cn.cong.util.RequiredPermission;
import com.github.pagehelper.PageInfo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.FileFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private IEmployeeService employeeService;

    @Autowired
    private IDepartmentService departmentService;

    @Autowired
    private IRoleService roleService;

    @RequestMapping("/list")
    @RequiredPermission(name = "员工查询", expression = "employee:list")
    public String list(@ModelAttribute("qo") EmployeeQueryObject qo, Model model) {
        PageInfo<Employee> query = employeeService.query(qo);
        model.addAttribute("pageInfo", query);
        List<Department> departments = departmentService.selectAll();
        model.addAttribute("departments", departments);
        return "/employee/list";
    }

    @RequestMapping("/input")
    @RequiredPermission(name = "员工编辑", expression = "employee:saveOrUpdate")
    public String input(Long id, Model model) {
        List<Department> departments = departmentService.selectAll();
        model.addAttribute("departments", departments);
        List<Role> roles = roleService.selectAll();
        model.addAttribute("roles", roles);
        if (id != null) {
            Employee employee = employeeService.selectById(id);
            model.addAttribute("employee", employee);
            List<Role> ownRoleNames = employeeService.ownRoleNames(id);
            model.addAttribute("ownRoleNames", ownRoleNames);
        }
        return "employee/input";
    }

    @RequestMapping("/saveOrUpdate")
    @RequiredPermission(name = "员工编辑", expression = "employee:saveOrUpdate")
    @ResponseBody
    public JsonResult saveOrUpdate(Employee employee, Long[] roleIds) {
        int i = 1/0;
        try {
            if (employee.getId() != null) {
                employeeService.update(employee, roleIds);
            } else {
                employeeService.insert(employee, roleIds);
            }
            return new JsonResult(true, "编辑成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new JsonResult(false, "编辑失败");
        }
    }

    @RequestMapping("/delete")
    @RequiredPermission(name = "员工删除", expression = "employee:delete")
    public String delete(Long id) {
        employeeService.deleteEmployeeRole(id);
        employeeService.delete(id);
        return "redirect:/employee/list";
    }

    @RequestMapping("/checkUsername")
    @ResponseBody
    public Map<String, Boolean> checkUsername(String username) {
        // 若存在 返回 true, 若不存在 false
        boolean exist = employeeService.checkUsername(username);
        HashMap<String, Boolean> data = new HashMap<>();
        /**
         * - valid: true   代表验证通过（该用户名不存在）
         * - valid: false  代表验证不通过（用户名已经存在）
        * */
        data.put("valid", !exist);
        return data;
    }

    @RequestMapping("/exportXls")
    public void exportXls(HttpServletResponse response) throws Exception {
        // 文件下载的响应头（让浏览器访问资源的时候一下载的方式打开）
        response.setHeader("Content-Disposition",
                "attachment;filename=employee.xls");
        // 创建 excel 文件
        Workbook wb = employeeService.exportXls();
        // 把 excel 的数据输出给浏览器
        wb.write(response.getOutputStream());
    }

    @RequestMapping("/importXls")
    public String importXls(MultipartFile file) throws Exception {
        Workbook wb = new HSSFWorkbook(file.getInputStream());
        employeeService.importXls(wb);
        return "redirect:/employee/list";
    }
}
