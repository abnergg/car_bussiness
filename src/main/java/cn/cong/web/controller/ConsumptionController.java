package cn.cong.web.controller;

import cn.cong.domain.*;
import cn.cong.qo.ConsumptionQueryObject;
import cn.cong.qo.JsonResult;
import cn.cong.qo.QueryObject;
import cn.cong.service.*;
import cn.cong.util.RequiredPermission;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/consumption")
public class ConsumptionController {

    @Autowired
    private IConsumptionService consumptionService;

    @Autowired
    private IBusinessService businessService;

    @Autowired
    private ISystemDictionaryService systemDictionaryService;

    @Autowired
    private IAppointmentService appointmentService;

    @Autowired
    private IConsumptionItemService consumptionItemService;

    @RequestMapping("/list")
    @RequiredPermission(name = "结算单查询", expression = "consumption:list")
    public String list(@ModelAttribute("qo") ConsumptionQueryObject qo, Model model) {
        PageInfo<Consumption> query = consumptionService.query(qo);
        model.addAttribute("pageInfo", query);
        List<Business> businesses = businessService.selectAll();
        model.addAttribute("businesses", businesses);
        List<SystemDictionary> systemDictionaries = systemDictionaryService.queryBusinessNameByParentId(72);
        model.addAttribute("systemDictionaries", systemDictionaries);
        return "/consumption/list";
    }

    @RequestMapping("/input")
    @RequiredPermission(name = "结算单编辑", expression = "consumption:saveOrUpdate")
    public String input(Long id, Model model) {
        List<Business> businesses = businessService.selectAll();
        model.addAttribute("businesses", businesses);
        List<SystemDictionary> systemDictionaries = systemDictionaryService.queryBusinessNameByParentId(1);
        model.addAttribute("systemDictionaries",systemDictionaries);
        List<SystemDictionary> systemDictionarieSettlements = systemDictionaryService.queryBusinessNameByParentId(13);
        model.addAttribute("systemDictionarieSettlements",systemDictionarieSettlements);
        if (id != null) {
            Consumption consumption = consumptionService.selectById(id);
            List<ConsumptionItem> consumptionItems = consumptionItemService.selectByCon(consumption.getCno());
            model.addAttribute("consumptionItems",consumptionItems);
            model.addAttribute("consumption", consumption);
        }
        return "consumption/input";
    }

    @RequestMapping("/input1")
    @RequiredPermission(name = "结算单编辑", expression = "consumption:saveOrUpdate")
    public String input(Long aid, Model model, int status) {
        long id = consumptionService.insertByAid(aid);
        appointmentService.updateStatusById(status,aid);
        return "redirect:/consumption/input?id=" + id;
    }

    @RequestMapping("/saveOrUpdate")
    @RequiredPermission(name = "结算单编辑", expression = "consumption:saveOrUpdate")
    public String saveOrUpdate(Consumption consumption) {
        if (consumption.getId() != null) {
            consumptionService.update(consumption);
        }
        return "redirect:/consumption/input?id="+consumption.getId();
    }

    @RequestMapping("/consume")
    @RequiredPermission(name = "结算更新", expression = "consumption:saveOrUpdate")
    public String consume(Consumption consumption) {
        consumptionService.updateStatusConsumeIdById(consumption);
        return "redirect:/consumption/input?id="+consumption.getId();
    }

    @RequestMapping("/audit")
    @RequiredPermission(name = "结算更新", expression = "consumption:saveOrUpdate")
    public String audit(Consumption consumption) {

        consumptionService.updateStatusAuditIdById(consumption);
        return "redirect:/consumption/input?id="+consumption.getId();
    }

    @RequestMapping("/delete")
    @RequiredPermission(name = "结算单删除", expression = "consumption:delete")
    public String delete(Long id) {
        consumptionService.delete(id);
        return "redirect:/consumption/list";
    }
}
