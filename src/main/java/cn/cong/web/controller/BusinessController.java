package cn.cong.web.controller;

import cn.cong.domain.Business;
import cn.cong.qo.BusinessQueryObject;
import cn.cong.qo.QueryObject;
import cn.cong.service.IBusinessService;
import cn.cong.service.ISystemDictionaryService;
import cn.cong.util.FileUploadUtil;
import cn.cong.util.RequiredPermission;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/business")
public class BusinessController {

    @Autowired
    private IBusinessService businessService;

    @RequestMapping("/list")
    @RequiredPermission(name = "门店查询", expression = "business:list")
    public String list(@ModelAttribute("qo") BusinessQueryObject qo, Model model) {
        PageInfo<Business> query = businessService.query(qo);
        model.addAttribute("pageInfo", query);
        return "/business/list";
    }

    @RequestMapping("/input")
    @RequiredPermission(name = "门店编辑", expression = "business:saveOrUpdate")
    public String input(Long id, Model model) {
        if (id != null) {
            Business business = businessService.selectById(id);
            model.addAttribute("business", business);
        }
        return "business/input";
    }

    @RequestMapping("/saveOrUpdate")
    @RequiredPermission(name = "门店编辑", expression = "business:saveOrUpdate")
    public String saveOrUpdate(Business business, MultipartFile file, HttpServletRequest request) throws Exception {

        if (file != null && file.getSize() > 0) {
            String path = request.getServletContext().getRealPath("/static");
            String filePath = FileUploadUtil.uploadFile(file, path);
            business.setLicenseImg("/static" + filePath);
        }

        if (business.getId() != null) {
            businessService.update(business);
        } else {
            businessService.insert(business);
        }
        return "redirect:/business/list";
    }

    @RequestMapping("/delete")
    @RequiredPermission(name = "门店删除", expression = "business:delete")
    public String delete(Long id) {
        businessService.delete(id);
        return "redirect:/business/list";
    }

}
