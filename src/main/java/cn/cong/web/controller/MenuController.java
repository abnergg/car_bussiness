package cn.cong.web.controller;

import cn.cong.domain.Menu;
import cn.cong.domain.Permission;
import cn.cong.qo.JsonResult;
import cn.cong.qo.MenuQueryObject;
import cn.cong.service.IMenuService;
import cn.cong.service.IPermissionService;
import cn.cong.util.RequiredPermission;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/systemMenu")
public class MenuController {

    @Autowired
    private IMenuService menuService;

    @Autowired
    private IPermissionService permissionService;

    @RequestMapping("/list")
    @RequiredPermission(name = "菜单管理", expression = "employee:list")
    public String list(@ModelAttribute("qo") MenuQueryObject qo, Model model) {
        PageInfo<Menu> pageInfo = menuService.query(qo);
        model.addAttribute("pageInfo", pageInfo);
        List<Menu> parentMenus = menuService.queryParent();
        model.addAttribute("parentMenus", parentMenus);
        List<Permission> permissions = permissionService.selectAll();
        model.addAttribute("permissions", permissions);
        return "/systemMenu/list";
    }

    @RequestMapping("/updateOrSave")
    @RequiredPermission(name = "菜单管理", expression = "employee:list")
    @ResponseBody
    public JsonResult updateOrSave(Menu menu, Long[] expressionIds) {
        if (menu.getId() != null) {
            menuService.update(menu, expressionIds);
            return new JsonResult(true,"修改成功");
        } else {
            menuService.insert(menu, expressionIds);
            return new JsonResult(true,"添加成功");
        }
    }

    @RequestMapping("/updateStatus")
    @RequiredPermission(name = "菜单管理", expression = "employee:list")
    public String updateStatus(Long id,boolean status) {
        menuService.updateStatusByMenuId(id,status);
        return "redirect:/systemMenu/list";
    }

    @RequestMapping("/treeData")
    @ResponseBody
    public List<Map> treeData(){
        List<Map> maps = menuService.queryTreeData();
        return maps;
    }
}
