package cn.cong.web.controller;

import cn.cong.domain.Department;
import cn.cong.qo.CustomerReportQueryObject;
import cn.cong.qo.QueryObject;
import cn.cong.service.IBusinessService;
import cn.cong.service.ICustomerReportService;
import cn.cong.service.IDepartmentService;
import cn.cong.util.RequiredPermission;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/customerReport")
public class CustomerReportController {

    @Autowired
    private ICustomerReportService customerReportService;

    @Autowired
    private IBusinessService businessService;

    @RequestMapping("/list")
    @RequiredPermission(name="收入报表", expression="customerReport:list")
    public String list(@ModelAttribute("qo") CustomerReportQueryObject qo, Model model){
        PageInfo<Map<String, Object>> query = customerReportService.query(qo);
        model.addAttribute("PageInfo",query);
        model.addAttribute("businesses",businessService.selectAll());
        return "/businessReport/list";
    }

    @RequestMapping("/chart")
    @RequiredPermission(name="柱状图", expression="customerReport:chart")
    public String chart(CustomerReportQueryObject qo,Model model){
        PageInfo<Map<String,Object>> pageInfo = customerReportService.queryByGroupType(qo);
        List<Object> pageTypeList = new ArrayList<>();
        List<Object> countList = new ArrayList<>();
        List<Object> totalAmountList = new ArrayList<>();
        List<Object> payAmountList = new ArrayList<>();
        List<Object> discountAmountList = new ArrayList<>();
        for (Map<String, Object> map : pageInfo.getList()) {
            pageTypeList.add(map.get("groupType"));
            countList.add(map.get("count"));
            totalAmountList.add(map.get("totalAmount"));
            payAmountList.add(map.get("payAmount"));
            discountAmountList.add(map.get("discountAmount"));
        }
        model.addAttribute("groupType", JSON.toJSONString(pageTypeList));
        model.addAttribute("count",JSON.toJSONString(countList));
        model.addAttribute("totalAmount",JSON.toJSONString(totalAmountList));
        model.addAttribute("payAmount",JSON.toJSONString(payAmountList));
        model.addAttribute("discountAmount",JSON.toJSONString(discountAmountList));
        return "/businessReport/charts";
    }
}
