package cn.cong.web.controller;

import cn.cong.domain.MessageBoard;
import cn.cong.domain.SystemDictionary;
import cn.cong.qo.MessageBoardQueryObject;
import cn.cong.qo.QueryObject;
import cn.cong.service.IMessageBoardService;
import cn.cong.service.ISystemDictionaryService;
import cn.cong.util.RequiredPermission;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/messageBoard")
public class MessageBoardController {

    @Autowired
    private IMessageBoardService messageBoardService;

    @Autowired
    private ISystemDictionaryService systemDictionaryService;

    @RequestMapping("/list")
    @RequiredPermission(name="留言查询", expression="messageBoard:list")
    public String list(MessageBoardQueryObject qo, Model model){
        PageInfo<MessageBoard> query = messageBoardService.query(qo);
        model.addAttribute("pageInfo",query);
        List<SystemDictionary> systemDictionaries = systemDictionaryService.queryBusinessNameByParentId(1);
        model.addAttribute("systemDictionaries",systemDictionaries);
        return "/messageBoard/list";
    }

    @RequestMapping("/detail")
    public String detail(Long id,Model model){
        List<Map<String,Object>> list = messageBoardService.selectReplyByBoardId(id);
        model.addAttribute("list",list);
        MessageBoard messageBoard = messageBoardService.selectById(id);
        model.addAttribute("messageBoard",messageBoard);
        return "/messageBoard/detail";
    }

    @RequestMapping("/save")
    public String save(MessageBoard messageBoard){
        messageBoardService.saveMessageBoard(messageBoard);
        return "redirect:/messageBoard/list";
    }

    /*@RequestMapping("/input")
    @RequiredPermission(name="留言编辑", expression="messageBoard:saveOrUpdate")
    public String input(Long id,Model model){
        if (id!=null) {
            MessageBoard messageBoard = messageBoardService.selectById(id);
            model.addAttribute("messageBoard",messageBoard);
        }
        return "messageBoard/input";
    }

    @RequestMapping("/saveOrUpdate")
    @RequiredPermission(name="留言编辑", expression="messageBoard:saveOrUpdate")
    public String saveOrUpdate(MessageBoard messageBoard){
        if (messageBoard.getId()!=null) {
            messageBoardService.update(messageBoard);
        }else {
            messageBoardService.insert(messageBoard);
        }
        return "redirect:/messageBoard/list";
    }

    @RequestMapping("/delete")
    @RequiredPermission(name="留言删除", expression="messageBoard:delete")
    public String delete(Long id){
        messageBoardService.delete(id);
        return "redirect:/messageBoard/list";
    }*/
}
