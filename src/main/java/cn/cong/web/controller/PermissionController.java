package cn.cong.web.controller;

import cn.cong.domain.Permission;
import cn.cong.qo.JsonResult;
import cn.cong.qo.QueryObject;
import cn.cong.service.IPermissionService;
import cn.cong.util.RequiredPermission;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.reflect.Method;
import java.util.*;

@Controller
@RequestMapping("/permission")
public class PermissionController {

    @Autowired
    private IPermissionService permissionService;

    @Autowired
    private ApplicationContext ctx;

    @RequestMapping("/list")
    public String list(QueryObject qo, Model model) {
        PageInfo<Permission> query = permissionService.query(qo);
        model.addAttribute("pageInfo", query);
        return "/permission/list";
    }


    @RequestMapping("/load")
    @ResponseBody
    @RequiredPermission(name = "权限重新加载", expression = "permission:load")
    public JsonResult load() {

        try {
            //如何知道所有控制器的处理方法，转成成数据库权限表中的一条条数据
            Map<String, Object> map = ctx.getBeansWithAnnotation(Controller.class);
            // 定义一个存权限 List 集合
            Set<Permission> permissions = new LinkedHashSet<>();
            // 获取所有控制器对象
            Collection<Object> collections = map.values();
            List<Permission> allPermissions = permissionService.selectAll();
            for (Object collection : collections) {
                // 获取控制器对象本类中的方法
                Method[] methods = collection.getClass().getDeclaredMethods();
                for (Method method : methods) {
                    // 获取方法的注释
                    RequiredPermission annotation = method.getAnnotation(RequiredPermission.class);
                    if (annotation != null) {
                        Permission permission = new Permission();
                        permission.setName(annotation.name());
                        permission.setExpression(annotation.expression());

                        // 存到 set 之前判断一下，看这个权限是否在数据库已存在，若不存在才存到 set, 之后插入
                        if (!allPermissions.contains(permission)) {
                            permissions.add(permission);
                        }
                    }
                }
            }
            if (permissions.size() > 0) {
                permissionService.batchSave(permissions);
            }
            return new JsonResult(true, "加载成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return new JsonResult(false, "加载失败！");
        }
    }

    /*@RequestMapping("/input")
    public String input(Long id, Model model) {
        if (id != null) {
            Permission permission = permissionService.selectById(id);
            model.addAttribute("permission", permission);
        }
        return "permission/input";
    }

    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(Permission permission) {
        if (permission.getId() != null) {
            permissionService.update(permission);
        } else {
            permissionService.insert(permission);
        }
        return "redirect:/permission/list";
    }

    @RequestMapping("/delete")
    public String delete(Long id) {
        permissionService.delete(id);
        return "redirect:/permission/list";
    }*/
}
