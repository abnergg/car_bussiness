package cn.cong.web.controller;

import cn.cong.domain.Permission;
import cn.cong.domain.Role;
import cn.cong.qo.QueryObject;
import cn.cong.service.IPermissionService;
import cn.cong.service.IRoleService;
import cn.cong.util.RequiredPermission;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IPermissionService permissionService;

    @RequestMapping("/list")
    @RequiredPermission(name="角色查询", expression="role:list")
    public String list(QueryObject qo,Model model){
        PageInfo<Role> query = roleService.query(qo);
        model.addAttribute("pageInfo",query);
        return "/role/list";
    }

    @RequestMapping("/input")
    @RequiredPermission(name="角色编辑", expression="role:saveOrUpdate")
    public String input(Long id,Model model){
        List<Permission> permissions = permissionService.selectAll();
        model.addAttribute("permissions",permissions);
        if (id!=null) {
            Role role = roleService.selectById(id);
            model.addAttribute("role",role);
            List<Permission> ownPermissionNames =  roleService.queryByRoleId(id);
            model.addAttribute("ownPermissionNames",ownPermissionNames);
        }
        return "role/input";
    }

    @RequestMapping("/saveOrUpdate")
    @RequiredPermission(name="角色编辑", expression="role:saveOrUpdate")
    public String saveOrUpdate(Role role, Long[] permissionIds){
        if (role.getId()!=null) {
            roleService.update(role,permissionIds);
        }else {
            roleService.insert(role, permissionIds);
        }
        return "redirect:/role/list";
    }

    @RequestMapping("/delete")
    @RequiredPermission(name="角色删除", expression="role:delete")
    public String delete(Long id){
        roleService.delete(id);
        return "redirect:/role/list";
    }
}
