package cn.cong.web.controller;

import cn.cong.domain.Business;
import cn.cong.domain.SystemDictionary;
import cn.cong.service.IBusinessService;
import cn.cong.service.ISystemDictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class IndexController {

    @Autowired
    private IBusinessService businessService;

    @Autowired
    private ISystemDictionaryService systemDictionaryService;

    @RequestMapping("/index")
    public String index(Model model){
        Business business = businessService.queryMainstore(1);
        model.addAttribute("business",business);
        List<SystemDictionary> businessName = systemDictionaryService.queryBusinessNameByParentId(1l);
        model.addAttribute("Business",businessName);
        return "/index";
    }
}
