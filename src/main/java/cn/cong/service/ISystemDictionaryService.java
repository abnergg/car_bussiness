package cn.cong.service;

import cn.cong.domain.SystemDictionary;
import cn.cong.qo.QueryObject;
import cn.cong.qo.SystemDictionaryQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

public interface ISystemDictionaryService {
    int insert(SystemDictionary systemDictionary);

    int delete(Long id);

    int update(SystemDictionary systemDictionary);

    SystemDictionary selectById(Long id);

    List<SystemDictionary> selectAll();

    PageInfo<SystemDictionary> query(SystemDictionaryQueryObject qo);

    List<Map> queryTreeData();

    List<SystemDictionary> queryBusinessNameByParentId(long ParentId);
}