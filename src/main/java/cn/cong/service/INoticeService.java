package cn.cong.service;

import cn.cong.domain.Notice;
import cn.cong.qo.NoticeQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface INoticeService {
    int insert(Notice notice);

    int delete(Long id);

    int update(Notice notice);

    Notice selectById(Long id);

    List<Notice> selectAll();

    PageInfo<Notice> query(NoticeQueryObject qo);

    void updateStatus(Long noticeId, Integer status);

    void updateReadStatus(Long noticeId);

    void insertEmployeeNotice(Long id);
}