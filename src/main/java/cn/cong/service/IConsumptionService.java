package cn.cong.service;

import cn.cong.domain.Consumption;
import cn.cong.qo.ConsumptionQueryObject;
import cn.cong.qo.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IConsumptionService {
    int insert(Consumption consumption);

    int delete(Long id);

    int update(Consumption consumption);

    Consumption selectById(Long id);

    List<Consumption> selectAll();

    PageInfo<Consumption> query(ConsumptionQueryObject qo);

    long insertByAid(Long aid);

    void updateStatusConsumeIdById(Consumption consumption);

    void updateStatusAuditIdById(Consumption consumption);
}