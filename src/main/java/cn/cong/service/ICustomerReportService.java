package cn.cong.service;

import cn.cong.domain.Consumption;
import cn.cong.qo.AppointmentQueryObject;
import cn.cong.qo.ConsumptionQueryObject;
import cn.cong.qo.CustomerReportQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

public interface ICustomerReportService {

    PageInfo<Map<String ,Object>> query(CustomerReportQueryObject qo);

    PageInfo<Map<String, Object>> queryByGroupType(CustomerReportQueryObject qo);
}