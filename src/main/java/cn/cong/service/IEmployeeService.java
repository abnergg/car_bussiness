package cn.cong.service;

import cn.cong.domain.Employee;
import cn.cong.domain.Role;
import cn.cong.qo.EmployeeQueryObject;
import com.github.pagehelper.PageInfo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

public interface IEmployeeService {
    int delete(Long id);
    int insert(Employee employee, Long[] roleIds);
    Employee selectById(Long id);
    List<Employee> selectAll();
    int update(Employee employee, Long[] roleIds);
    PageInfo<Employee> query(EmployeeQueryObject eqo);

    List<Role> ownRoleNames(Long id);

    void deleteEmployeeRole(Long employeeId);

    boolean checkUsername(String username);

    Employee login(String username, String password);

    Workbook exportXls();

    void importXls(Workbook wb);
}