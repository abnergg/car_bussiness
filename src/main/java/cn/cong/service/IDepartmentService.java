package cn.cong.service;

import cn.cong.domain.Department;
import cn.cong.qo.PageResult;
import cn.cong.qo.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IDepartmentService {
    int insert(Department department);

    int delete(Long id);

    int update(Department department);

    Department selectById(Long id);

    List<Department> selectAll();

    PageInfo<Department> query(QueryObject qo);
}