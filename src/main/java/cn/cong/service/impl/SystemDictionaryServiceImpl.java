package cn.cong.service.impl;

import cn.cong.domain.SystemDictionary;
import cn.cong.mapper.SystemDictionaryMapper;
import cn.cong.qo.QueryObject;
import cn.cong.qo.SystemDictionaryQueryObject;
import cn.cong.service.ISystemDictionaryService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SystemDictionaryServiceImpl implements ISystemDictionaryService {

    @Autowired
    private SystemDictionaryMapper systemDictionaryMapper;

    @Override
    public int insert(SystemDictionary systemDictionary) {
        int i = systemDictionaryMapper.insert(systemDictionary);
        return i;
    }

    @Override
    public int delete(Long id) {
        int i = systemDictionaryMapper.deleteByPrimaryKey(id);
        return i;
    }

    @Override
    public int update(SystemDictionary systemDictionary) {
        int i = systemDictionaryMapper.updateByPrimaryKey(systemDictionary);
        return i;
    }

    @Override
    public SystemDictionary selectById(Long id) {
        SystemDictionary systemDictionary = systemDictionaryMapper.selectByPrimaryKey(id);
        return systemDictionary;
    }

    @Override
    public List<SystemDictionary> selectAll() {
        List<SystemDictionary> list = systemDictionaryMapper.selectAll();
        return list;
    }

    @Override
    public PageInfo<SystemDictionary> query(SystemDictionaryQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<SystemDictionary> systemDictionarys = systemDictionaryMapper.selectForList(qo);
        return new PageInfo<>(systemDictionarys);
    }

    @Override
    public List<SystemDictionary> queryBusinessNameByParentId(long ParentId) {
        return systemDictionaryMapper.selectBusinessNameByParentId(ParentId);
    }

    @Override
    public List<Map> queryTreeData() {
        List<SystemDictionary> selectAll = systemDictionaryMapper.selectAll();
        List<SystemDictionary> top = new ArrayList<>();
        for (SystemDictionary systemDictionary : selectAll) {
            if (systemDictionary.getParentId() == null) {
                top.add(systemDictionary);
                systemDictionary.setChildren(findChildren(selectAll, systemDictionary.getId()));
            }
        }
        fillChildren(selectAll, top);
        String str = JSON.toJSONString(top);
        str = str.replaceAll("name", "text").replaceAll("children", "nodes");
        List<Map> maps = JSON.parseArray(str, Map.class);

        return maps;
    }

    //查找是否有下一级
    private void fillChildren(List<SystemDictionary> selectAll, List<SystemDictionary> children) {
        if (children == null || children.size() == 0) {
            return;
        }
        for (SystemDictionary item : children) {
            item.setChildren(findChildren(selectAll,item.getId()));
            fillChildren(selectAll,item.getChildren());
        }
    }

    private List<SystemDictionary> findChildren(List<SystemDictionary> selectAll, Long parentId) {
        List<SystemDictionary> children = new ArrayList<>();
        for (SystemDictionary systemDictionary : selectAll) {
            if (systemDictionary.getParentId() == parentId) {
                children.add(systemDictionary);
            }
        }
        return children;
    }
}
