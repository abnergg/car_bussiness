package cn.cong.service.impl;

import cn.cong.domain.Consumption;
import cn.cong.mapper.CustomerReportMapper;
import cn.cong.qo.AppointmentQueryObject;
import cn.cong.qo.ConsumptionQueryObject;
import cn.cong.qo.CustomerReportQueryObject;
import cn.cong.service.ICustomerReportService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class CustomerReportServiceImpl implements ICustomerReportService {

    @Autowired
    private CustomerReportMapper customerReportMapper;

    @Override
    public PageInfo<Map<String ,Object>> query(CustomerReportQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<Map<String,Object>> list = customerReportMapper.selectForList(qo);
        return new PageInfo<>(list);
    }

    @Override
    public PageInfo<Map<String, Object>> queryByGroupType(CustomerReportQueryObject qo) {
        qo.setPageSize(Integer.MAX_VALUE);
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<Map<String,Object>> list = customerReportMapper.selectForListByGroupType(qo.getGroupType());
        return new PageInfo<>(list);
    }
}
