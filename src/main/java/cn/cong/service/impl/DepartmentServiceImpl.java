package cn.cong.service.impl;

import cn.cong.domain.Department;
import cn.cong.mapper.DepartmentMapper;
import cn.cong.qo.PageResult;
import cn.cong.qo.QueryObject;
import cn.cong.service.IDepartmentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DepartmentServiceImpl implements IDepartmentService {

    @Autowired
    private DepartmentMapper departmentMapper;

    @Override
    public int insert(Department department) {
        int i = departmentMapper.insert(department);
        return i;
    }

    @Override
    public int delete(Long id) {
        int i = departmentMapper.deleteByPrimaryKey(id);
        return i;
    }

    @Override
    public int update(Department department) {
        int i = departmentMapper.updateByPrimaryKey(department);
        return i;
    }

    @Override
    public Department selectById(Long id) {
        Department department = departmentMapper.selectByPrimaryKey(id);
        return department;
    }

    @Override
    public List<Department> selectAll() {
        List<Department> list = departmentMapper.selectAll();
        return list;
    }

    @Override
    public PageInfo<Department> query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<Department> departments = departmentMapper.selectForList(qo);
        return new PageInfo<>(departments);
    }
}
