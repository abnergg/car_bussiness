package cn.cong.service.impl;

import cn.cong.domain.MessageBoard;
import cn.cong.mapper.MessageBoardMapper;
import cn.cong.qo.MessageBoardQueryObject;
import cn.cong.qo.QueryObject;
import cn.cong.service.IMessageBoardService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class MessageBoardServiceImpl implements IMessageBoardService {

    @Autowired
    private MessageBoardMapper messageBoardMapper;

    @Override
    public int insert(MessageBoard messageBoard) {
        int i = messageBoardMapper.insert(messageBoard);
        return i;
    }

    @Override
    public int delete(Long id) {
        int i = messageBoardMapper.deleteByPrimaryKey(id);
        return i;
    }

    @Override
    public int update(MessageBoard messageBoard) {
        int i = messageBoardMapper.updateByPrimaryKey(messageBoard);
        return i;
    }

    @Override
    public MessageBoard selectById(Long id) {
        MessageBoard messageBoard = messageBoardMapper.selectByPrimaryKey(id);
        return messageBoard;
    }

    @Override
    public List<MessageBoard> selectAll() {
        List<MessageBoard> list = messageBoardMapper.selectAll();
        return list;
    }

    @Override
    public PageInfo<MessageBoard> query(MessageBoardQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize(),"create_time desc");
        List<MessageBoard> messageBoards = messageBoardMapper.selectForList(qo);
        return new PageInfo<>(messageBoards);
    }

    @Override
    public List<Map<String, Object>> selectReplyByBoardId(Long id) {
        List<Map<String,Object>> list = messageBoardMapper.selectMessageReplyByBoardId(id);
        return list;
    }

    @Override
    public void saveMessageBoard(MessageBoard messageBoard) {
        messageBoard.setReplystatus(false);
        messageBoard.setCreateTime(new Date());
        messageBoardMapper.insert(messageBoard);
    }
}
