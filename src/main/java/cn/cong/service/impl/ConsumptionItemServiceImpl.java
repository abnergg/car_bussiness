package cn.cong.service.impl;

import cn.cong.domain.ConsumptionItem;
import cn.cong.domain.Employee;
import cn.cong.mapper.ConsumptionItemMapper;
import cn.cong.qo.QueryObject;
import cn.cong.service.IConsumptionItemService;
import cn.cong.util.UserContext;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ConsumptionItemServiceImpl implements IConsumptionItemService {

    @Autowired
    private ConsumptionItemMapper consumptionItemMapper;

    @Override
    public int insert(ConsumptionItem consumptionItem) {
        consumptionItem.setCreateTime(new Date());
        Employee employee = (Employee) UserContext.getSession().getAttribute("EMPLOYEE_IN_SESSION");
        consumptionItem.setCreateUserId(employee.getId());
        int i = consumptionItemMapper.insert(consumptionItem);
        return i;
    }

    @Override
    public int delete(Long[] ids) {
        int i = 0;
        for (Long id : ids) {
            i = consumptionItemMapper.deleteByPrimaryKey(id);
        }
        return i;
    }

    @Override
    public int update(ConsumptionItem consumptionItem) {
        int i = consumptionItemMapper.updateByPrimaryKey(consumptionItem);
        return i;
    }

    @Override
    public ConsumptionItem selectById(Long id) {
        ConsumptionItem consumptionItem = consumptionItemMapper.selectByPrimaryKey(id);
        return consumptionItem;
    }

    @Override
    public List<ConsumptionItem> selectAll() {
        List<ConsumptionItem> list = consumptionItemMapper.selectAll();
        return list;
    }

    @Override
    public List<ConsumptionItem> selectByCon(String cno) {
        List<ConsumptionItem> consumptionItems = consumptionItemMapper.selectByCno(cno);
        return consumptionItems;
    }


}
