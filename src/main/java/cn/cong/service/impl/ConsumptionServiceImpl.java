package cn.cong.service.impl;

import cn.cong.domain.Appointment;
import cn.cong.domain.Consumption;
import cn.cong.domain.Employee;
import cn.cong.mapper.ConsumptionMapper;
import cn.cong.qo.ConsumptionQueryObject;
import cn.cong.qo.QueryObject;
import cn.cong.service.IAppointmentService;
import cn.cong.service.IConsumptionService;
import cn.cong.util.UserContext;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
@Transactional
public class ConsumptionServiceImpl implements IConsumptionService {

    @Autowired
    private ConsumptionMapper consumptionMapper;

    @Autowired
    private IAppointmentService appointmentService;

    @Override
    public int insert(Consumption consumption) {
        consumption.setStatus(68);
        consumption.setCreateTime(new Date());
        Employee employee = (Employee) UserContext.getSession().getAttribute("EMPLOYEE_IN_SESSION");
        consumption.setCreateUserId(employee.getId());
        // 生成流水单号的随机数
        String date = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        int a = new Random().nextInt(90000) + 10000;
        consumption.setCno(date + a);
        int i = consumptionMapper.insert(consumption);
        return i;
    }

    @Override
    public int delete(Long id) {
        int i = consumptionMapper.deleteByPrimaryKey(id);
        return i;
    }

    @Override
    public int update(Consumption consumption) {
        int i = consumptionMapper.updateByPrimaryKey(consumption);
        return i;
    }

    @Override
    public Consumption selectById(Long id) {
        Consumption consumption = consumptionMapper.selectByPrimaryKey(id);
        return consumption;
    }

    @Override
    public List<Consumption> selectAll() {
        List<Consumption> list = consumptionMapper.selectAll();
        return list;
    }

    @Override
    public PageInfo<Consumption> query(ConsumptionQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<Consumption> consumptions = consumptionMapper.selectForList(qo);
        return new PageInfo<>(consumptions);
    }

    @Override
    public long insertByAid(Long aid) {
        Consumption consumption = new Consumption();
        Appointment appointment = appointmentService.selectById(aid);
        consumption.setCreateTime(appointment.getCreateTime());
        Employee employee = (Employee) UserContext.getSession().getAttribute("EMPLOYEE_IN_SESSION");
        consumption.setCreateUserId(employee.getId());
        // 生成流水单号的随机数
        String date = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        int a = new Random().nextInt(90000) + 10000;
        consumption.setCno(date + a);
        consumption.setAppointmentAno(appointment.getAno());
        consumption.setInfo(appointment.getInfo());
        consumption.setCustomerName(appointment.getContactName());
        consumption.setCustomerTel(appointment.getContactTel());
        consumption.setBusiness(appointment.getBusiness());
        consumption.setStatus(68);
        consumptionMapper.insert(consumption);
        long id = consumption.getId();
        return id;
    }

    @Override
    public void updateStatusConsumeIdById(Consumption consumption) {
        consumption.setStatus(69);
        consumption.setPayTime(new Date());
        consumptionMapper.updateStatusConsumeIdById(consumption);
    }

    @Override
    public void updateStatusAuditIdById(Consumption consumption) {
        consumption.setStatus(70);
        consumption.setAuditTime(new Date());
        consumptionMapper.updateStatusAuditIdById(consumption);
    }
}
