package cn.cong.service.impl;

import cn.cong.domain.Business;
import cn.cong.mapper.BusinessMapper;
import cn.cong.qo.BusinessQueryObject;
import cn.cong.qo.QueryObject;
import cn.cong.service.IBusinessService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BusinessServiceImpl implements IBusinessService {

    @Autowired
    private BusinessMapper businessMapper;

    @Override
    public int insert(Business business) {
        int i = businessMapper.insert(business);
        return i;
    }

    @Override
    public int delete(Long id) {
        int i = businessMapper.deleteByPrimaryKey(id);
        return i;
    }

    @Override
    public int update(Business business) {
        int i = businessMapper.updateByPrimaryKey(business);
        return i;
    }

    @Override
    public Business selectById(Long id) {
        Business business = businessMapper.selectByPrimaryKey(id);
        return business;
    }

    @Override
    public List<Business> selectAll() {
        List<Business> list = businessMapper.selectAll();
        return list;
    }

    @Override
    public PageInfo<Business> query(BusinessQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize(),"open_Date desc");
        List<Business> businesss = businessMapper.selectForList(qo);
        return new PageInfo<>(businesss);
    }

    @Override
    public Business queryMainstore(int mainStoreId) {
        return businessMapper.selectBusinessByMainStoreId(mainStoreId);
    }

}
