package cn.cong.service.impl;

import cn.cong.domain.Permission;
import cn.cong.domain.Role;
import cn.cong.mapper.RoleMapper;
import cn.cong.qo.QueryObject;
import cn.cong.service.IRoleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RoleServiceImpl implements IRoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public int insert(Role role, Long[] permissionIds) {
        int i = roleMapper.insert(role);
        if (permissionIds != null && permissionIds.length > 0) {
            for (Long permissionId : permissionIds) {
                roleMapper.insertRelation(role.getId(),permissionId);
            }
        }
        return i;
    }

    @Override
    public int delete(Long id) {
        int i = roleMapper.deleteByPrimaryKey(id);
        roleMapper.deleteRolePermission(id);
        return i;
    }

    @Override
    public int update(Role role, Long[] permissionIds) {
        roleMapper.deleteRolePermission(role.getId());
        if (permissionIds != null && permissionIds.length > 0) {
            for (Long permissionId : permissionIds) {
                roleMapper.insertRelation(role.getId(),permissionId);
            }
        }
        int i = roleMapper.updateByPrimaryKey(role);
        return i;
    }

    @Override
    public Role selectById(Long id) {
        Role role = roleMapper.selectByPrimaryKey(id);
        return role;
    }

    @Override
    public List<Role> selectAll() {
        List<Role> list = roleMapper.selectAll();
        return list;
    }

    @Override
    public PageInfo<Role> query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<Role> roles = roleMapper.selectForList(qo);
        return new PageInfo<>(roles);
    }

    @Override
    public List<Permission> queryByRoleId(Long id) {

        return roleMapper.selectByRoleId(id);
    }
}
