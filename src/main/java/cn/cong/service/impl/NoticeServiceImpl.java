package cn.cong.service.impl;

import cn.cong.domain.Employee;
import cn.cong.domain.Notice;
import cn.cong.mapper.NoticeMapper;
import cn.cong.qo.NoticeQueryObject;
import cn.cong.service.INoticeService;
import cn.cong.util.UserContext;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class NoticeServiceImpl implements INoticeService {

    @Autowired
    private NoticeMapper noticeMapper;

    @Override
    public int insert(Notice notice) {
        Employee employee = (Employee) UserContext.getSession().getAttribute("EMPLOYEE_IN_SESSION");
        if (employee != null) {
            notice.setCreatId(employee.getId());
        }
        notice.setCreatTime(new Date());
        notice.setStatus(false);
        notice.setReadStatus(false);
        int i = noticeMapper.insert(notice);
        return i;
    }

    @Override
    public int delete(Long id) {
        int i = noticeMapper.deleteByPrimaryKey(id);
        return i;
    }

    @Override
    public int update(Notice notice) {
        int i = noticeMapper.updateByPrimaryKey(notice);
        return i;
    }

    @Override
    public Notice selectById(Long id) {
        Notice notice = noticeMapper.selectByPrimaryKey(id);
        return notice;
    }

    @Override
    public List<Notice> selectAll() {
        List<Notice> list = noticeMapper.selectAll();
        return list;
    }

    @Override
    public PageInfo<Notice> query(NoticeQueryObject qo) {
        Employee employee = (Employee) UserContext.getSession().getAttribute("EMPLOYEE_IN_SESSION");
        List<Map<String, Object>> readStatus = new ArrayList<>();
        Long employeeId = 0l;
        if (employee != null) {
            if (!employee.isAdmin()) {
                qo.setStatus(true);
            }
            employeeId = employee.getId();
        }else {
            qo.setStatus(true);
        }
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<Notice> notices = noticeMapper.selectForList(qo);
        for (Notice notice : notices) {
            readStatus = noticeMapper.selectStatusByEmployeeNotice(notice.getId(), employeeId);
            if (readStatus.size() == 0) {
                notice.setReadStatus(false);
            } else {
                notice.setReadStatus(true);
            }
        }
        return new PageInfo<>(notices);
    }

    @Override
    public void updateStatus(Long noticeId, Integer status) {
        noticeMapper.updateStatus(noticeId, status);
    }

    @Override
    public void updateReadStatus(Long noticeId) {
        noticeMapper.updateReadStatus(noticeId);
    }

    @Override
    public void insertEmployeeNotice(Long id) {
        Employee employee = (Employee) UserContext.getSession().getAttribute("EMPLOYEE_IN_SESSION");
        Long employeeId = 0l;
        if (employee != null) {
            employeeId = employee.getId();
        }
        List<Map<String, Object>> readStatus = noticeMapper.selectStatusByEmployeeNotice(id, employeeId);
        if (readStatus.size() == 0) {
            noticeMapper.insertEmployeeNotice(id, employeeId);
        }
    }
}
