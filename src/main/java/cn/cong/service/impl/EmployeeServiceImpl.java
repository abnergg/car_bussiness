package cn.cong.service.impl;

import cn.cong.domain.Employee;
import cn.cong.domain.Role;
import cn.cong.mapper.EmployeeMapper;
import cn.cong.qo.EmployeeQueryObject;
import cn.cong.service.IEmployeeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EmployeeServiceImpl implements IEmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public int delete(Long id) {
        int i = employeeMapper.deleteByPrimaryKey(id);
        return i;
    }

    @Override
    public int insert(Employee employee, Long[] roleIds) {
        int insert = employeeMapper.insert(employee);
        if (roleIds != null && roleIds.length > 0) {
            for (Long roleId : roleIds) {
                employeeMapper.insertRelation(employee.getId(), roleId);
            }
        }
        return insert;
    }

    @Override
    public Employee selectById(Long id) {
        Employee employee = employeeMapper.selectByPrimaryKey(id);
        return employee;
    }

    @Override
    public List<Employee> selectAll() {
        List<Employee> employees = employeeMapper.selectAll();
        return employees;
    }

    @Override
    public int update(Employee employee, Long[] roleIds) {
        employeeMapper.deleteByEmployeeId(employee.getId());
        if (roleIds != null && roleIds.length > 0) {
            for (Long roleId : roleIds) {
                employeeMapper.insertRelation(employee.getId(), roleId);
            }
        }
        int i = employeeMapper.updateByPrimaryKey(employee);
        return i;
    }

    @Override
    public PageInfo<Employee> query(EmployeeQueryObject eqo) {
        PageHelper.startPage(eqo.getCurrentPage(), eqo.getPageSize());
        List<Employee> employees = employeeMapper.selectForList(eqo);
        return new PageInfo<>(employees);
    }

    @Override
    public List<Role> ownRoleNames(Long id) {
        return employeeMapper.selectRolesByRoleId(id);
    }

    @Override
    public void deleteEmployeeRole(Long employeeId) {
        employeeMapper.deleteByEmployeeId(employeeId);
    }

    @Override
    public boolean checkUsername(String username) {
        Employee employee = employeeMapper.checkUsername(username);
        return employee != null;
    }

    @Override
    public Employee login(String username, String password) {
        Employee employee = employeeMapper.login(username, password);
        return employee;
    }

    @Override
    public Workbook exportXls() {
        // 创建 excel 文件
        Workbook wb = new HSSFWorkbook();
        // 创建 sheet
        Sheet sheet = wb.createSheet("员工名单");
        // 标题行
        Row row = sheet.createRow(0);
        //设置内容到单元格中
        row.createCell(0).setCellValue("名字");
        row.createCell(1).setCellValue("邮箱");
        row.createCell(2).setCellValue("年龄");
        // 查询员工数据
        List<Employee> employees = employeeMapper.selectAll();
        for (int i = 0; i < employees.size(); i++) {
            Employee employee = employees.get(i);
            // 创建行（每个员工就是一行）
            row = sheet.createRow(i + 1);
            row.createCell(0).setCellValue(employee.getName());
            row.createCell(1).setCellValue(employee.getEmail());
            row.createCell(2).setCellValue(employee.getAge());
        }
        return wb;
    }

    @Override
    public void importXls(Workbook wb) {
        Sheet sheet = wb.getSheetAt(0);
        int lastRowNum = sheet.getLastRowNum();
        for (int i = 1; i <= lastRowNum; i++) {
            Employee employee = new Employee();
            Row row = sheet.getRow(i);
            employee.setName(row.getCell(0).getStringCellValue());
            employee.setEmail(row.getCell(1).getStringCellValue());
            employee.setAge((int)row.getCell(2).getNumericCellValue());
            employee.setPassword("1");
            employeeMapper.insert(employee);
        }
    }
}
