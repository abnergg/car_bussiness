package cn.cong.service.impl;

import cn.cong.domain.Menu;
import cn.cong.domain.Permission;
import cn.cong.domain.SystemDictionary;
import cn.cong.mapper.MenuMapper;
import cn.cong.mapper.PermissionMapper;
import cn.cong.qo.MenuQueryObject;
import cn.cong.service.IMenuService;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class MenuServiceImpl implements IMenuService {

    @Autowired
    private MenuMapper menuMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public int insert(Menu menu, Long[] permissionIds) {
        menu.setCreateTime(new Date());
        menu.setStatus(false);
        Long parentId = menu.getParentId();
        if (parentId != null) {
            Menu menu1 = menuMapper.selectByPrimaryKey(parentId);
            menu.setParentName(menu1.getName());
        }
        int i = menuMapper.insert(menu);
        menuMapper.deletePermissionMenuByMenuId(menu.getId());
        if (permissionIds != null) {
            for (Long permissionId : permissionIds) {
                menuMapper.insertByPermissionId(permissionId, menu.getId());
            }
        }
        return i;
    }

    @Override
    public int delete(Long id) {
        int i = menuMapper.deleteByPrimaryKey(id);
        return i;
    }

    @Override
    public int update(Menu menu, Long[] permissionIds) {
        int i = menuMapper.updateByPrimaryKey(menu);
        menuMapper.deletePermissionMenuByMenuId(menu.getId());
        if (permissionIds != null) {
            for (Long permissionId : permissionIds) {
                menuMapper.insertByPermissionId(permissionId, menu.getId());
            }
        }
        return i;
    }

    @Override
    public Menu selectById(Long id) {
        Menu menu = menuMapper.selectByPrimaryKey(id);
        return menu;
    }

    @Override
    public List<Menu> selectAll() {
        List<Menu> list = menuMapper.selectAll();
        return list;
    }

    @Override
    public PageInfo<Menu> query(MenuQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<Menu> menus = menuMapper.selectForList(qo);
        for (Menu menu : menus) {
            List<Permission> permissions = permissionMapper.selectByMenuId(menu.getId());
            menu.setPermissions(permissions);
        }
        return new PageInfo<>(menus);
    }

    @Override
    public List<Menu> queryParent() {
        List<Menu> menus = menuMapper.selectParent();
        return menus;
    }

    @Override
    public void updateStatusByMenuId(Long id, boolean status) {
        menuMapper.updateStatusByMenuId(id, status);
    }

    @Override
    public List<Map> queryTreeData() {

        List<Menu> selectAll = menuMapper.selectAll();
        List<Menu> top = new ArrayList<>();
        for (Menu menu : selectAll) {
            if (menu.getParentId() == null) {
                top.add(menu);
                menu.setChildren(findChildren(selectAll,menu.getId()));
            }
        }
        fillChildren(selectAll, top);
        String str = JSON.toJSONString(top);
//        str = str.replaceAll("name", "text").replaceAll("children", "nodes");
        List<Map> maps = JSON.parseArray(str, Map.class);

        return maps;
    }

    //查找是否有下一级
    private void fillChildren(List<Menu> selectAll, List<Menu> children) {
        if (children == null || children.size() == 0) {
            return;
        }
        for (Menu child : children) {
            child.setChildren(findChildren(selectAll,child.getId()));
            fillChildren(selectAll,child.getChildren());
        }
    }

    private List<Menu> findChildren(List<Menu> selectAll, Long id) {
        List<Menu> children = new ArrayList<>();
        for (Menu menu : selectAll) {
            if (menu.getParentId() == id) {
                children.add(menu);
            }
        }
        return children;
    }
}
