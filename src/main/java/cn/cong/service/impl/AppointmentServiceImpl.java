package cn.cong.service.impl;

import cn.cong.domain.Appointment;
import cn.cong.mapper.AppointmentMapper;
import cn.cong.qo.AppointmentQueryObject;
import cn.cong.qo.QueryObject;
import cn.cong.service.IAppointmentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class AppointmentServiceImpl implements IAppointmentService {

    @Autowired
    private AppointmentMapper appointmentMapper;

    @Override
    public int insert(Appointment appointment) {
        appointment.setCreateTime(new Date());
        int i = appointmentMapper.insert(appointment);
        return i;
    }

    @Override
    public int delete(Long id) {
        int i = appointmentMapper.deleteByPrimaryKey(id);
        return i;
    }

    @Override
    public int update(Appointment appointment) {
        int i = appointmentMapper.updateByPrimaryKey(appointment);
        return i;
    }

    @Override
    public Appointment selectById(Long id) {
        Appointment appointment = appointmentMapper.selectByPrimaryKey(id);
        return appointment;
    }

    @Override
    public List<Appointment> selectAll() {
        List<Appointment> list = appointmentMapper.selectAll();
        return list;
    }

    @Override
    public PageInfo<Appointment> query(AppointmentQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize(),"appointment_time desc");
        List<Appointment> appointments = appointmentMapper.selectForList(qo);
        return new PageInfo<>(appointments);
    }

    @Override
    public void updateStatusById(int status, Long id) {
        appointmentMapper.updateStatusById(status,id);
    }
}
