package cn.cong.service.impl;

import cn.cong.domain.Permission;
import cn.cong.mapper.PermissionMapper;
import cn.cong.qo.QueryObject;
import cn.cong.service.IPermissionService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
@Transactional
public class PermissionServiceImpl implements IPermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public int insert(Permission permission) {
        int i = permissionMapper.insert(permission);
        return i;
    }

    @Override
    public int delete(Long id) {
        int i = permissionMapper.deleteByPrimaryKey(id);
        return i;
    }

    @Override
    public int update(Permission permission) {
        int i = permissionMapper.updateByPrimaryKey(permission);
        return i;
    }

    @Override
    public Permission selectById(Long id) {
        Permission permission = permissionMapper.selectByPrimaryKey(id);
        return permission;
    }

    @Override
    public List<Permission> selectAll() {
        List<Permission> list = permissionMapper.selectAll();
        return list;
    }

    @Override
    public PageInfo<Permission> query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<Permission> permissions = permissionMapper.selectForList(qo);
        return new PageInfo<>(permissions);
    }

    @Override
    public void batchSave(Set<Permission> permissions) {
        permissionMapper.batchInsert(permissions);
    }

    @Override
    public List<String> queryExpressionByEmployeeId(Long employeeId) {
        List<String> permissions = permissionMapper.selectExpressionByEmployeeId(employeeId);
        return permissions;
    }
}
