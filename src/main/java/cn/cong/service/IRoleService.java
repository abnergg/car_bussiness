package cn.cong.service;

import cn.cong.domain.Permission;
import cn.cong.domain.Role;
import cn.cong.qo.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IRoleService {
    int insert(Role role, Long[] permissionIds);

    int delete(Long id);

    int update(Role role, Long[] permissionIds);

    Role selectById(Long id);

    List<Role> selectAll();

    PageInfo<Role> query(QueryObject qo);

    List<Permission> queryByRoleId(Long id);
}