package cn.cong.service;

import cn.cong.domain.Business;
import cn.cong.qo.BusinessQueryObject;
import cn.cong.qo.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IBusinessService {
    int insert(Business business);

    int delete(Long id);

    int update(Business business);

    Business selectById(Long id);

    List<Business> selectAll();

    PageInfo<Business> query(BusinessQueryObject qo);

    Business queryMainstore(int mainStoreId);
}