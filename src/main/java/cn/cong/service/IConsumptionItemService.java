package cn.cong.service;

import cn.cong.domain.ConsumptionItem;
import cn.cong.qo.ConsumptionItemQueryObject;
import cn.cong.qo.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IConsumptionItemService {
    int insert(ConsumptionItem consumptionItem);

    int delete(Long[] id);

    int update(ConsumptionItem consumptionItem);

    ConsumptionItem selectById(Long id);

    List<ConsumptionItem> selectAll();

    List<ConsumptionItem> selectByCon(String cno);
}