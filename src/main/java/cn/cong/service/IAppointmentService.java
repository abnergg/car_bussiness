package cn.cong.service;

import cn.cong.domain.Appointment;
import cn.cong.qo.AppointmentQueryObject;
import cn.cong.qo.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IAppointmentService {
    int insert(Appointment appointment);

    int delete(Long id);

    int update(Appointment appointment);

    Appointment selectById(Long id);

    List<Appointment> selectAll();

    PageInfo<Appointment> query(AppointmentQueryObject qo);

    void updateStatusById(int status, Long id);
}