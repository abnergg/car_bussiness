package cn.cong.service;

import cn.cong.domain.MessageBoard;
import cn.cong.qo.MessageBoardQueryObject;
import cn.cong.qo.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

public interface IMessageBoardService {
    int insert(MessageBoard messageBoard);

    int delete(Long id);

    int update(MessageBoard messageBoard);

    MessageBoard selectById(Long id);

    List<MessageBoard> selectAll();

    PageInfo<MessageBoard> query(MessageBoardQueryObject qo);

    List<Map<String, Object>> selectReplyByBoardId(Long id);

    void saveMessageBoard(MessageBoard messageBoard);
}