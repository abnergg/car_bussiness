package cn.cong.service;

import cn.cong.domain.Menu;
import cn.cong.qo.MenuQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

public interface IMenuService {
    int insert(Menu menu, Long[] permissionIds);

    int delete(Long id);

    int update(Menu menu, Long[] permissionIds);

    Menu selectById(Long id);

    List<Menu> selectAll();

    PageInfo<Menu> query(MenuQueryObject qo);

    List<Menu> queryParent();

    void updateStatusByMenuId(Long id, boolean status);

    List<Map> queryTreeData();

}