package cn.cong.service;

import cn.cong.domain.Permission;
import cn.cong.qo.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Set;

public interface IPermissionService {
    int insert(Permission permission);

    int delete(Long id);

    int update(Permission permission);

    Permission selectById(Long id);

    List<Permission> selectAll();

    PageInfo<Permission> query(QueryObject qo);

    void batchSave(Set<Permission> permissions);

    List<String> queryExpressionByEmployeeId(Long employeeId);
}