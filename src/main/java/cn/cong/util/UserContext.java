package cn.cong.util;

import cn.cong.domain.Employee;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public abstract class UserContext {

    public static final String EMPLOYEE_IN_SESSION = "EMPLOYEE_IN_SESSION";

    public static final String EXPRESSIONS_IN_SESSION = "EXPRESSIONS_IN_SESSION";
    // 获取请求对象
    public static HttpServletRequest getRequest(){
        ServletRequestAttributes attributes = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
        return attributes.getRequest();
    }

    // 获取 session 对象
    public static HttpSession getSession(){
        return getRequest().getSession();
    }

    //往 session 存登录员工对象
    public static void setEmployee(Employee employee){
        getSession().setAttribute(EMPLOYEE_IN_SESSION,employee);
    }

    //往 session 存权限字符串
    public static void setExpressions(List<String> expressions){
        getSession().setAttribute(EXPRESSIONS_IN_SESSION,expressions);
    }

}
