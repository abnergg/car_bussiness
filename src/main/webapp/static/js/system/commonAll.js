$(function () {
    $('.btn-delete').click(function () {
        var url = $(this).data('url');
        Swal.fire({
            title: '你确定要删除吗?',
            text: "你将无法还原!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '确定删除!',
            cancelButtonText: '我按错了!'
        }).then((result) => {
            if (result.value) {
                // 点了确定做什么，由开发者决定
                location.href = url;
            }
        });
    });
});

