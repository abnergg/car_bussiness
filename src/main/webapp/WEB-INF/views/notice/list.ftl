<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>公告通知管理</title>
    <#include "/common/link.ftl">
    <script>
        $(function () {
            $('.input-date').datepicker({
                format: 'yyyy-mm-dd', //格式
                language: 'zh-CN', //中文
                autoclose: true,//选择后自动关闭
                //showMeridian:true, //是否显示上下午
                minView: 2,//精确到哪位
            });
            var val = !$('#currentSession').val();
            console.log(val);
            if (val){
                $('.btn-lll').remove();
            }
        });
    </script>
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="notice"/>
    <#include "/common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>公告通知管理</h1>
        </section>
        <input type="hidden" id="currentSession" value="${EMPLOYEE_IN_SESSION.admin}">
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <div style="margin: 20px 0px 0px 10px">
                    <form class="form-inline" id="searchForm" action="/notice/list" method="post">
                        <input type="hidden" name="currentPage" id="currentPage" value="1">

                           <div class="form-group">
                                <label>创建日期查询：</label>
                                <input placeholder="请输入开始日期" type="text" class="form-control input-date" name="startDate" value="${(qo.startDate?string("yyyy-MM-dd"))!}"/> -
                                <input placeholder="请输入结束日期" type="text" class="form-control input-date" name="endDate" value="${(qo.endDate?string("yyyy-MM-dd"))!}"/>
                            </div>

                        <div class="form-group">
                            <label>公告级别：</label>
                            <select class="form-control" name="level" id="level">
                                <option value="">请选择级别</option>
                                <option value="0">紧急</option>
                                <option value="1">重要</option>
                                <option value="2">普通</option>
                            </select>
                            <script>
                                $('#level').val(${qo.level!});
                            </script>
                        </div>
                        <div class="form-group">
                            <label>阅读状态：</label>
                            <input type="hidden" name="employeeId" value="${EMPLOYEE_IN_SESSION.id}">
                            <select class="form-control" name="readStatus" id="readStatus">
                                <option value="">全部</option>
                                <option value="0">未读</option>
                                <option value="1">已读</option>
                            </select>
                            <script>
                                $('#readStatus').val(${(qo.readStatus?string('1','0'))!});
                            </script>
                        </div>

                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 查询</button>

                            <a href="#" class="btn btn-success btn-input btn-lll">
                                <span class="glyphicon glyphicon-plus"></span> 添加
                            </a>
                    </form>

                </div>
                <div class="box-body table-responsive">
                <table class="table table-hover table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>公告标题</th>
                        <th>创建人</th>
                        <th>创建时间</th>
                        <th>公告级别</th>
                        <th>是否已读</th>
                        <th class="btn-lll">发布状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <#list pageInfo.list as p>
                        <tr>
                            <td>${p_index+1}</td>
                            <td>${p.info!}</td>
                            <td>${p.creatName!}</td>
                            <td>${(p.creatTime?string("yyyy-MM-dd"))!}</td>
                            <td>${p.levelDisplay()!}</td>
                            <td><span style="${(p.readStatus?string("color:green","color:red"))!}">${(p.readStatus?string("已读","未读"))!}</span></td>
                            <td class="btn-lll">${(p.status?string("已发布","未发布"))}</td>
                            <td>
                                <a class="btn btn-success btn-xs  btn-show" href="/notice/checked?id=${p.id}" >
                                    <span class="glyphicon glyphicon-phone-alt"></span> 查看</a>
                                <a class="btn btn-primary btn-xs btn-input btn-lll" data-json='${p.toJson()}'>
                                    <span class="glyphicon glyphicon-pencil"></span> 编辑
                                </a>
                                <a class="btn btn-xs btn-info btn-push btn-lll" href="/notice/updateStatus?noticeId=${p.id}&status=1">
                                    <span class="glyphicon glyphicon-share"></span> 发布</a>
                                <a class="btn btn-xs btn-danger btn-remove btn-lll" href="/notice/updateStatus?noticeId=${p.id}&status=0">
                                    <span class="glyphicon glyphicon-remove"></span> 取消</a>
                            </td>
                        </tr>
                    </#list>
                    </tbody>
                </table>
                <#include "/common/page.ftl">
                </div>
            </div>
        </section>
    </div>
    <#include "/common/footer.ftl">
</div>

<!--模态框-->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">新增/编辑</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="editForm" action="/notice/saveOrUpdate" method="post" >
                <div class="modal-body">
                    <input type="hidden" name="id" id="modalId">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">公告标题：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control"  placeholder="请输入公告标题" name="title" id="modalTitle"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">公告内容：</label>
                        <div class="col-sm-7">
                            <textarea type="text" class="form-control" rows="8" name="info" id="modalInfo"
                                      placeholder="请输入公告内容"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">公告级别：</label>
                        <div class="col-sm-7">
                            <select class="form-control" name="level" id="modalLevel">
                                <option value="">请选择级别</option>
                                <option value="0">紧急</option>
                                <option value="1">重要</option>
                                <option value="2">普通</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary btn-submit">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(".btn-input").click(function () {
        var json = $(this).data('json');
        if (json){
            $('#modalId').val(json.id);
            $('#modalInfo').val(json.info);
            $('#modalTitle').val(json.title);
            $('#modalLevel').val(json.level);
        }
        $("#editModal").modal('show')
    })
</script>

</body>
</html>
