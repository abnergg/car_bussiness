<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>叩丁狼互联网商户平台</title>
  <#include "/common/link.ftl"/>
  <link rel="stylesheet" href="/static/css/index_css.css">
</head>
<body>
<div class="nav">
  <div class="nav-div">
    <div class="row justify-content-between" >
      <div class="col-md-4 border-right" style="padding: 0px;" >
        <h4>${business.name}</h4>
        <h5 style="color:#434343;font-size: 18px">连锁运营顾问 / 高级服务专家</h5>
      </div>
      <div class="col-md-3 category" style="padding-left: 30px">
        <#list Business as name>
          <button class="btn btn-primary-cus ">${name.name}</button>
        </#list>
      </div>
      <div class="col-md-1" style="text-align: center;padding: 20px;">
        <button class="btn btn-primary-full" type="button" id="btn-appointment">
          马上预约
        </button>

      </div>
      <div class="col-md-4 tel"  style="padding: 15px;">
        <h2 style="vertical-align:middle;"><i class="fa fa-phone"></i></h2>
        <h5 style="color:#434343; ">全国免费热线:</h5>
        <h2 >020-85628002</h2>
      </div>
    </div>
  </div>

</div>
<div class="body">
  <div class="banner">
    <img src="/static/img/system/banner.png" />
  </div>
</div>
<div class="footer">
  <div class="row">
    <div class="col-sm-4">
      <div class="position-relative p-3 bg-white " style="height: 250px">
        <blockquote><strong>店铺信息</strong></blockquote>
        <small style="line-height:30px">BUSINESS INFORMATION</small>
        <p class="text-muted">
          ${business.intro}
        </p>
      </div>
    </div>
    <div class="col-sm-4 ">
      <div class="position-relative p-3 bg-white " style="height: 250px">
        <blockquote><strong>服务与支持</strong></blockquote>
        <small style="line-height:30px">SERVICE AND SUPPORT</small>
        <p class="text-muted">${business.scope}</p>
      </div>
    </div>
    <div class="col-sm-4 ">
      <div class="position-relative p-3 bg-white" style="height: 250px">
        <blockquote><strong>联系我们</strong></blockquote>
        <small style="line-height:30px">CONCAT US</small>
        <h5 class="text-muted" ><strong style="font-size: 17px">联系电话：</strong>${business.tel}</h5>
        <h5 class="text-muted" ><strong style="font-size: 17px">联系地址：</strong>${business.address}</h5>
      </div>
    </div>
  </div>

</div>

<!-- Modal -->



<div class="modal fade" id="appointmentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog " style="max-width: 380px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" >请输入预约信息</h4>
      </div>
      <form id="appointmentForm" method="post" action="/appointment/save">
        <div class="modal-body">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i> </span>
            <input  class="form-control" placeholder="请输入您的姓名">
          </div>
          <br/>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
            <input  class="form-control" placeholder="请输入您的电话">
          </div>
          <br/>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-home"></i></span>
            <select class="form-control" placeholder="请选择预约门店">
              <option value="">请选择预约门店</option>
              <option value="1">叩丁狼互联网汽车服务平台</option>
              <option value="1">叩丁狼互联网汽车服务平台</option>
              <option value="1">叩丁狼互联网汽车服务平台</option>
              <option value="1">叩丁狼互联网汽车服务平台</option>
            </select>
          </div>
          <br/>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-tag"></i></span>
            <select class="form-control"  placeholder="请选择预约业务">
              <option value="">请选择预约业务</option>
              <option value="1">售卖</option>
              <option value="2">保养</option>
              <option value="3">修理</option>
              <option value="4">美容</option>
              <option value="5">配件</option>
              <option value="6">订货</option>
              <option value="7">售后</option>
            </select>
          </div>
          <br/>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input  class="form-control"  placeholder="请选择预约时间">
          </div>
          <br/>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-bookmark"></i></span>
            <textarea  class="form-control" placeholder="备注说明"></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary-full">确定预约</button>
        </div>
      </form>
    </div>
  </div>
</div>


</body>
</html>
