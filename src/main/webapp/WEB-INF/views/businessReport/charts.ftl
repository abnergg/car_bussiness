<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="/static/js/plugins/echarts/echarts.common.min.js"></script>

</head>
<body>
<div id="main" style="width: 1200px;height:400px;"></div>
<script>
    var myChart = echarts.init(document.getElementById('main'));
    var option = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                crossStyle: {
                    color: '#999'
                }
            }
        },
        toolbox: {
            feature: {
                dataView: {show: true, readOnly: false},
                magicType: {show: true, type: ['line', 'bar']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        legend: {
            data: ['总消费金额', '总实收金额', '总优惠金额', '总订单数']
        },
        xAxis: [
            {
                type: 'category',
                data: ${groupType},
                axisPointer: {
                    type: 'shadow'
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                name: '金额',
                min: 0,
                max: 300000,
                interval: 50000,
                axisLabel: {
                    formatter: '{value} ￥'
                }
            },
            {
                type: 'value',
                name: '总条数',
                min: 0,
                max: 25,
                interval: 5,
                axisLabel: {
                    formatter: '{value} 条'
                }
            },

        ],
        series: [
            {
                name: '总消费金额',
                type: 'bar',
                data: ${totalAmount},
                markPoint: {
                    data: [
                        {type: 'max', name: '最大值'},
                        {type: 'min', name: '最小值'}
                    ]
                },
                markLine: {
                    data: [
                        {type: 'average', name: '平均值'}
                    ]
                }
            },
            {
                name: '总实收金额',
                type: 'bar',
                data: ${payAmount}
            },
            {
                name: '总优惠金额',
                type: 'bar',
                data: ${discountAmount}
            },
            {
                name: '总订单数',
                type: 'line',
                yAxisIndex: 1,
                data: ${count}
            }
        ]
    };

    myChart.setOption(option);
</script>
</body>
</html>