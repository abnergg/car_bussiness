<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>权限管理</title>
    <#include "/common/link.ftl">
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="permission"/>
    <#include "/common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>权限管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <form class="form-inline" id="searchForm" action="/permission/list" method="post">
                    <input type="hidden" name="currentPage" id="currentPage" value="1">
                    <a href="javascript:;" class="btn btn-success btn-reload" style="margin: 10px;">
                        <span class="glyphicon glyphicon-repeat"></span> 重新加载
                    </a>
                </form>

                <script>
                    $('.btn-reload').click(function () {
                        var id = $(this).data('id');
                        Swal.fire({
                            title: '你确定要重新加载吗?',
                            text:'你将无法还原!',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: '确定!',
                            cancelButtonText: '我按错了!'
                        }).then((result) => {
                            if (result.value) {
                                // 同步请求 location.href='/permission/load'

                                // 异步请求 post  '/permission/load'
                                $.post('/permission/load',function (data) {
                                    if (data.success) {
                                        Swal.fire({
                                            title: data.msg,
                                            confirmButtonText: `确定`,
                                        }).then((result) => {
                                            if (result.value) {
                                                location.href = '/permission/list';
                                            }
                                        })
                                    }else {
                                        Swal.fire(data.msg,'','info')
                                    }
                                });
                            }
                        });
                    });
                </script>

                <div class="box-body table-responsive ">
                    <table class="table table-hover table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>编号</th>
                            <th>权限名称</th>
                            <th>权限表达式</th>
                        </tr>
                        </thead>
                        <tbody>
                        <#list pageInfo.list as permission>
                            <tr>
                                <td>${permission_index + 1}</td>
                                <td>${permission.name}</td>
                                <td>${permission.expression}</td>
                            </tr>
                        </#list>
                        </tbody>
                    </table>
                    <#include "/common/page.ftl">
                </div>
            </div>
        </section>
    </div>
    <#include "/common/footer.ftl">
</div>
</body>
</html>
