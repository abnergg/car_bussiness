<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>门店管理</title>
    <#include "/common/link.ftl">
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="business"/>
    <#include "/common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>门店管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <div style="margin: 20px 0px 0px 10px">
                    <form class="form-inline" id="searchForm" action="/business/list" method="post">
                        <input type="hidden" name="currentPage" id="currentPage" value="1">
                                <div class="form-group">
                                    <label for="name">门店名称：</label>
                                    <input type="text" class="form-control" name="name" value="${qo.name}"
                                           placeholder="请输入门店名称">
                                </div>
                                <div class="form-group">
                                    <label for="scope">经营范围：</label>
                                    <input type="text" class="form-control" name="scope" value="${qo.scope}"
                                           placeholder="请输入经营范围">
                                </div>
                                <div class="form-group">
                                    <label for="tel">门店联系方式：</label>
                                    <input type="text" class="form-control" name="tel" value="${qo.tel}"
                                           placeholder="请输入门店联系方式">
                                </div>
                                <div class="form-group">
                                    <label for="legalName">根据法人查询：</label>
                                    <input type="text" class="form-control" name="legalName" value="${qo.legalName}"
                                           placeholder="请输入法人姓名">
                                </div>

                                <br/>
                                <br/>
                                <div class="form-group">
                                    <label>经营日期查询：</label>
                                    <input placeholder="请输入开始日期" type="text" class="form-control input-date" name="beginDate" value="${(qo.beginDate?string("yyyy-MM-dd"))!}" /> -
                                    <input placeholder="请输入结束日期" type="text" class="form-control input-date" name="endDate" value="${(qo.endDate?string("yyyy-MM-dd"))!}" />
                                </div>

                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 查询</button>

                                <a href="/business/input" class="btn btn-success">
                                    <span class="glyphicon glyphicon-plus"></span> 添加
                                </a>
                    </form>

                </div>
                <div class="box-body table-responsive">
                <table class="table table-hover table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th>门店名称</th>
                        <th>门店电话</th>
                        <th>门店地址</th>
                        <th>经营时间</th>
                        <th>门店性质</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <#list pageInfo.list as business>
                        <tr>
                            <td>${business_index + 1}</td>
                            <td>${business.name}</td>
                            <td>${business.tel}</td>
                            <td>${business.address}</td>
                            <td>${business.openDate?string("yyyy-MM-dd")!}</td>
                            <td>${((business.mainStore==1)?string("总店","分店"))!}
                                <button type="button" class="btn btn-warning storeDetails" data-json='${business.toJson()}'>门店详情</button>
                            </td>
                            <td>
                                <a href="/business/input?id=${business.id}" class="btn btn-info btn-xs">
                                    <span class="glyphicon glyphicon-pencil"></span> 编辑
                                </a>
                            </td>
                        </tr>
                    </#list>

                    </tbody>
                </table>
                <#include "/common/page.ftl">
                </div>
            </div>
        </section>
    </div>
    <#include "/common/footer.ftl">
</div>

<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <form class="form-horizontal" method="post" id="editForm">
                <div class="modal-body">
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label">门店名称：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="name" readonly>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label">门店性质：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="mainStore" readonly>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label">门店电话：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="tel" readonly>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label">门店地址：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="address" readonly>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label">经营时间：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="openDate" readonly>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label">门店介绍：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="intro" readonly>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label">经营范围：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="scope" readonly>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="sn" class="col-sm-3 control-label">经营许可证：</label>
                        <div class="col-sm-6">
                            <img id="licenseImg" width="300px">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="sn" class="col-sm-3 control-label">经营许可编码：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="licenseNumber" readonly>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="sn" class="col-sm-3 control-label">法人姓名：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="legalName" readonly>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="sn" class="col-sm-3 control-label">法人电话：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="legalTel" readonly>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('.storeDetails').click(function () {
        var data = $(this).data("json");
        if(data){
            $('#editForm input[name=name]').val(data.name);
            $('#editForm input[name=mainStore]').val((data.mainStore==1?"总店":"分店"));
            $('#editForm input[name=tel]').val(data.tel);
            $('#editForm input[name=address]').val(data.address);
            $('#editForm input[name=openDate]').val(data.openDate);
            $('#editForm input[name=intro]').val(data.intro);
            $('#editForm input[name=scope]').val(data.scope);
            $('#editForm input[name=licenseNumber]').val(data.licenseNumber);
            $('#editForm input[name=legalName]').val(data.legalName);
            $('#editForm input[name=legalTel]').val(data.legalTel);
            $('#licenseImg').attr('src',data.licenseImg);
        }
        $('#editModal').modal('show');
    });

    $('.input-date').datepicker({
        format:'yyyy-mm-dd', //格式
        language:'zh-CN', //中文
        autoclose: true,//选择后自动关闭
        //showMeridian:true, //是否显示上下午
        minView:2,//精确到哪位
    });
</script>

</body>
</html>
