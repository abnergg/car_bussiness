<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>字典目录管理</title>
    <#include "/common/link.ftl">

    <link rel="stylesheet" href="/static/js/plugins/bootstrap-treeview/bootstrap-treeview.min.css">
    <script src="/static/js/plugins/bootstrap-treeview/bootstrap-treeview.min.js"></script>

    <script>
        $(function () {
            var tree;
            $.get("/systemDictionary/treeData", function (data) {
                tree = $('#treeview-searchable').treeview({
                    color: "#428bca",
                    data: [{text: "数据字段目录", nodes:data}],
                    onNodeSelected: function(event, node) {
                        if (node.id == undefined) {
                            location.href = "/systemDictionary/list?nodeId=0";
                            return;
                        }
                        window.location.href = "/systemDictionary/list?parentId=" + node.id + "&nodeId=" + node.nodeId;
                    }
                });
                // 数据字典回显
                if ('${qo.nodeId}') {
                    //选中回显
                    tree.treeview('selectNode', [${qo.nodeId}, { silent: true }]);
                    tree.treeview('expandNode', [${qo.nodeId}, { silent: true }]);
                    if ('${qo.nodeId}'!=0){
                        // 当前节点
                        var parentNode = tree.treeview('getParent', ${qo.nodeId});
                        tree.treeview('expandNode', [parentNode, { silent: true }]);
                        while(parentNode.parentId){
                            parentNode = tree.treeview('getParent', parentNode.nodeId);
                            tree.treeview('expandNode', [parentNode, { silent: true }]);
                        }
                    }
                }

            });

            $('.btn-input').click(function () {
                // 清空表单数据
                $("#editForm").clearForm(true);

                if ('${qo.nodeId}') {
                    var node = tree.treeview('getNode', ${qo.nodeId});
                    $("#editForm input[name='parentName']").val(node.text);
                    $("#editForm input[name='parentId']").val(node.id);

                    var data = $(this).data("json");
                    if (data) {
                        $("#editForm input[name='id']").val(data.id);
                        $("#editForm input[name='name']").val(data.name);
                        $("#editForm input[name='intro']").val(data.intro);
                        $("#editForm input[name='sn']").val(data.sn);
                    }
                }else{
                    Swal.fire('操作失败','请先选择字典目录','error')
                    return
                }
                $('#editModal').modal('show');
            });

            $('.btn-submit').click(function () {
                $("#editForm").ajaxSubmit(function (data) {
                    if (data.success) {
                        location.href = "/systemDictionary/list?parentId=${qo.parentId}&nodeId=${qo.nodeId}";
                    }
                })
            });
        });
    </script>


</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">

    <#assign currentMenu="systemDictionary"/>

    <#include "/common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>数据字典管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <div class="row" style="margin:20px">
                    <div class="col-xs-4">
                        <div id="treeview-searchable" class="margin-left:30px;"></div>
                    </div>
                    <div class="col-xs-8">
                        <!--高级查询--->
                        <form class="form-inline" id="searchForm" action="/systemDictionary/list" method="post">
                            <input type="hidden" name="currentPage" id="currentPage" value="1">
                            <input type="hidden" name="nodeId" value="${qo.nodeId}">
                            <input type="hidden" name="parentId" value="${qo.parentId}">
                            <a href="#" class="btn btn-success btn-input" style="margin: 10px">
                                <span class="glyphicon glyphicon-plus"></span> 添加
                            </a>
                        </form>
                        <!--编写内容-->
                        <div class="box-body table-responsive no-padding ">
                            <table class="table table-hover table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>编号</th>
                                    <th>数据名称</th>
                                    <th>数据编码</th>
                                    <th>字典目录</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <#list pageInfo.list as s>
                                    <tr>
                                        <td>${s_index + 1}</td>
                                        <td>${s.name}</td>
                                        <td>${s.sn}</td>
                                        <td>${s.parentName}</td>
                                        <td>
                                            <a href="#" class="btn btn-info btn-xs btn-input" data-json='${s.toJSON()}'>
                                                <span class="glyphicon glyphicon-pencil"></span> 编辑
                                            </a>
                                            <a href="javascript:;" class="btn btn-danger btn-xs btn-delete"
                                               data-url="/systemDictionary/delete?id=${s.id}&nodeId=${qo.nodeId}&parentId=${qo.parentId}">
                                                <span class="glyphicon glyphicon-pencil"></span> 删除
                                            </a>
                                        </td>
                                    </tr>
                                </#list>

                                </tbody>
                            </table>
                            <#include "/common/page.ftl" >
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">新增/编辑</h4>
            </div>
            <form class="form-horizontal" action="/systemDictionary/saveOrUpdate" method="post" id="editForm">
                <div class="modal-body">
                    <input type="hidden" name="parentId">
                    <input type="hidden" name="id">
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label">字典目录：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="parentName" readonly>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label">名称：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="name"
                                   placeholder="请输入字典目录标题">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="sn" class="col-sm-3 control-label">编码：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="sn"
                                   placeholder="请输入字典目录编码">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="sn" class="col-sm-3 control-label">简介：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="intro"
                                   placeholder="请输入字典目录简介">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="button" class="btn btn-primary btn-submit">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>
