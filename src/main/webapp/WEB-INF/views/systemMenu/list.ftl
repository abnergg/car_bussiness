﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>菜单管理</title>
    <#include "/common/link.ftl">
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="systemMenu"/>
    <#include "/common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>菜单管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <div style="margin: 20px 0px 0px 10px">
                    <form class="form-inline" id="searchForm" action="/systemMenu/list" method="post">
                        <input type="hidden" name="currentPage" id="currentPage" value="1">
                        <div class="form-group">
                            <label>菜单名称</label>
                            <input type="text" class="form-control"  placeholder="请输入菜单名称" name="name" value="${qo.name}">
                        </div>
                            <div class="form-group">
                                <label>菜单地址</label>
                                <input type="text" class="form-control"  placeholder="请输入菜单地址" name="url" value="${qo.url}">
                            </div>
                            <div class="form-group">
                                <label>菜单状态</label>
                                <select class="form-control" name="status" id="status">
                                    <option value="">全部</option>
                                    <option value="1">启用</option>
                                    <option value="0">禁用</option>
                                </select>
                                <script>
                                    $('#status').val(${qo.status});
                                </script>
                            </div>
                            <div class="form-group">
                                <label>菜单类型</label>
                                <select class="form-control" name="parentId" id="parentId" >
                                    <option value="">全部</option>
                                    <option value="0">目录</option>
                                    <option value="1">菜单</option>
                                </select>
                                <script>
                                    $('#parentId').val(${qo.parentId});
                                </script>
                            </div>

                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 查询</button>

                        <a href="#" class="btn btn-success btn-input">
                            <span class="glyphicon glyphicon-plus"></span> 添加
                        </a>
                    </form>

                </div>
                <div class="box-body table-responsive ">
                <table class="table table-hover table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>菜单图标</th>
                        <th>菜单类型</th>
                        <th>菜单名称</th>
                        <th>上级菜单</th>
                        <th>菜单URL</th>
                        <th>权限表达式</th>
                        <th>排序</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <#list pageInfo.list as p>
                        <tr>
                            <td>${p.id!}</td>
                            <td> <i class="${p.icon!}"></i></td>
                            <td>${p.menuTypeDisplay()!}</td>
                            <td>${p.name!}</td>
                            <td>${p.parentName!}</td>
                            <td>${p.url!}</td>
                            <td>
                                <#list p.permissions as permission>
                                    ${permission.expression!}</br>
                                </#list>
                            </td>
                            <td>${p.sort!}</td>
                            <td>${((p.status)?string("启动","禁用"))!}</td>
                            <td>
                                <a href="#" class="btn btn-info btn-xs btn-input" data-json='${p.toJson()}'>
                                    <span class="glyphicon glyphicon-pencil"></span> 编辑
                                </a>
                                <a class="btn btn-xs btn-primary btn-status" href="/systemMenu/updateStatus?id=${p.id}&status=1">
                                    <span class="glyphicon glyphicon-phone-alt" ></span> 启用</a>
                                <a class="btn btn-xs btn-danger btn-status" href="/systemMenu/updateStatus?id=${p.id}&status=0">
                                    <span class="glyphicon glyphicon-remove"></span> 禁用</a>
                            </td>
                        </tr>
                    </#list>
                    </tbody>
                </table>
                    <!--分页-->
                    <#include "/common/page.ftl">
                </div>

            </div>
        </section>
    </div>
    <#include "/common/footer.ftl">
</div>

<script>

    $(".btn-input").click(function () {
        $("#editModal").modal('show')
    })
</script>
<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document" style="width: 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">新增/编辑</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="editForm" action="/systemMenu/updateOrSave" method="post" >
                <div class="modal-body" style="width: 1000px">
                    <input type="hidden" name="id" id="modalId" >
                    <input type="hidden" name="sn" id="modalSn" >
                    <input type="hidden" name="status" id="modalStatus" >
                    <input type="hidden" name="createTime" id="modalCreateTime" >
                    <input type="hidden" name="parentName" id="modalParentName" >
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">菜单类型：</label>
                        <div class="col-sm-7" id="menuType">
                            <label>
                                <input type="radio" name="a"  value="0" >
                                目录
                            </label>
                            <label>
                                <input type="radio" name="a"  value="1" checked>
                                菜单
                            </label>
                        </div>
                    </div>
                    <div class="form-group row" id="rowName">
                        <label class="col-sm-3 col-form-label">菜单名称：</label>
                        <div class="col-sm-7">
                            <input type="text"  class="form-control " placeholder="请输入菜单名称" name="name"/>
                        </div>
                    </div>
                    <div class="form-group row" id="rowParent">
                        <label class="col-sm-3 col-form-label">上级菜单：</label>
                        <div class="col-sm-7">
                            <select class="form-control" name="parentId" id="modalParentId">
                                <option value="">请选择上级菜单</option>
                                <#list parentMenus as pm>
                                    <option value="${pm.id}">${pm.name}</option>
                                </#list>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">菜单URL：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="url"
                                   placeholder="请输入菜单URL">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">菜单序号：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="sort"
                                   placeholder="请输入菜单序号">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">菜单图标：</label>
                        <div class="col-sm-7" style="color:lightslategrey">
                                <label>
                                    <input type="radio" name="icon"  value="fa fa-dashboard" checked>
                                    <i class="fa fa-dashboard"></i>
                                </label>
                                <label>
                                    <input type="radio" name="icon"  value="fa fa-pie-chart">
                                    <i class="fa fa-pie-chart"></i>
                                </label>
                                <label>
                                    <input type="radio" name="icon" value="fa fa-joomla" >
                                    <i class="fa fa-joomla"></i>
                                </label>
                                <label>
                                    <input type="radio" name="icon"  value="fa fa-files-o" >
                                    <i class="fa fa-files-o"></i>
                                </label>
                                <label>
                                    <input type="radio" name="icon"  value="fa fa-edit">
                                    <i class="fa fa-edit"></i>
                                </label>
                                <label>
                                    <input type="radio" name="icon" value="fa fa-laptop" >
                                    <i class="fa fa-laptop"></i>
                                </label>
                                <label>
                                    <input type="radio" name="icon" value="fa fa-circle-o" >
                                    <i class="fa fa-circle-o"></i>
                                </label>
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">权限表达式：</label>
                        <div class="col-sm-7">
                            <div class="row" style="margin-top: 10px;width: 1500px;margin-left:-300px">
                                <div class="col-sm-2 col-sm-offset-2" style="left: -10px">
                                    <select multiple class="form-control allMenu" style="height: 350px;width: 200px;" size="15">
                                        <#list permissions as per>
                                            <option value="${per.id}">${per.name}:${per.expression}</option>
                                        </#list>
                                    </select>
                                </div>

                                <div class="col-sm-1" style="margin-top: 60px;" >
                                    <div>

                                        <a type="button" class="btn btn-primary  " style="margin-top: 10px" title="右移动"
                                           onclick="moveSelected('allMenu', 'selfMenu')">
                                            <span class="glyphicon glyphicon-menu-right"></span>
                                        </a>
                                    </div>
                                    <div>
                                        <a type="button" class="btn btn-primary " style="margin-top: 10px" title="左移动"
                                           onclick="moveSelected('selfMenu', 'allMenu')">
                                            <span class="glyphicon glyphicon-menu-left"></span>
                                        </a>
                                    </div>
                                    <div>
                                        <a type="button" class="btn btn-primary " style="margin-top: 10px" title="全右移动"
                                           onclick="moveAll('allMenu', 'selfMenu')">
                                            <span class="glyphicon glyphicon-forward"></span>
                                        </a>
                                    </div>
                                    <div>
                                        <a type="button" class="btn btn-primary " style="margin-top: 10px" title="全左移动"
                                           onclick="moveAll('selfMenu', 'allMenu')">
                                            <span class="glyphicon glyphicon-backward"></span>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-sm-2" >
                                    <select multiple class="form-control selfMenu" name="expressionIds" style="height: 350px;" size="15">
                                    </select>
                                    <script>
                                        function moveSelected(srcClass, tageClass) {
                                            $('.' + tageClass).append($('.' + srcClass + ' > option:selected'));
                                        }

                                        function moveAll(srcClass, tageClass) {
                                            $('.' + tageClass).append($('.' + srcClass + ' > option'));
                                        }
                                    </script>
                                </div>
                            </div>
                            <script>
                                $(function () {
                                    $('.btn-input').click(function () {
                                        $('#editModal').clearForm(true);
                                        var json = $(this).data('json');
                                        if (json){
                                            var a;
                                            if (json.parentId){
                                                var modalParentId = json.parentId;
                                                if (modalParentId != null) {
                                                    a=1;
                                                    $('#rowName').after(ht);
                                                }else {
                                                    a=0;
                                                    $('#rowParent').remove();
                                                }

                                                $('#modalParentId').val(modalParentId);
                                            }
                                            $.each($('input[name="a"]'),function (i,Emle) {
                                                var val = $(Emle).val();
                                                if (val == a) {
                                                    $(Emle).prop("checked",true);
                                                }
                                            });

                                            $('#modalId').val(json.id);
                                            $('#modalSn').val(json.sn);
                                            $('#modalStatus').val(json.status);
                                            $('#modalCreateTime').val(json.createTime);
                                            $('#modalParentName').val(json.parentName);
                                            $('#editModal input[name="name"]').val(json.name);
                                            $('#editModal input[name="url"]').val(json.url);
                                            $('#editModal input[name="sort"]').val(json.sort);
                                            var icon = json.icon;
                                            $.each($('input[name="icon"]'),function (i,Emle) {
                                                var val = $(Emle).val();
                                                if (val == icon) {
                                                    $(Emle).prop("checked",true);
                                                }
                                            });
                                            var html;
                                            $.each(json.permissions,function (i,Elem) {
                                                html += "<option value='" + Elem.id + "'>" + Elem.name + "</option>";
                                            });
                                            $('select[name="expressionIds"]').html(html);
                                            var has = [];
                                            $('.selfMenu > option').each(function (i,Elem) {
                                                has.push($(Elem).val());
                                            });
                                            $('.allMenu > option').each(function (i,Elem) {
                                                var $option = $(Elem);
                                                var val = $option.val();
                                                if ($.inArray(val, has) >= 0) {
                                                    $option.remove();
                                                }
                                            });
                                        }
                                    });
                                    $('.btn-modalSubmit').click(function () {
                                        $.each($('select[name="expressionIds"] > option'),function (i,Elem) {
                                            if ($(Elem)){
                                                $(Elem).prop("selected",true);
                                            }
                                        });
                                        $('#editForm').ajaxSubmit(function (data) {
                                            if (data.success){
                                                window.location.href = "/systemMenu/list";
                                            }else {
                                                Swal.fire({
                                                    icon: 'error',
                                                    title:  data.msg,
                                                    text: 'Something went wrong!',
                                                })
                                            }
                                        });
                                    });

                                });
                            </script>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="button" class="btn btn-primary btn-modalSubmit">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    let ht = $('#rowParent').remove();
    $('.col-sm-7 input[name="a"]').change(function () {
        var val = $(this).val();
        if (val == 0){
            $('#rowParent').remove();
        }else {
            $('#rowName').after(ht);
        }
    });

</script>

</body>
</html>
