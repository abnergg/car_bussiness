﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>菜单管理</title>
    <#include "/common/link.ftl">
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="systemMenu"/>
    <#include "/common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>菜单管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <div style="margin: 20px 0px 0px 10px">
                    <form class="form-inline" id="searchForm" action="/xxxx" method="post">
                        <input type="hidden" name="currentPage" id="currentPage" value="1">
                        <div class="form-group">
                            <label>菜单名称</label>
                            <input type="text" class="form-control"  placeholder="请输入菜单名称">
                        </div>
                            <div class="form-group">
                                <label>菜单地址</label>
                                <input type="text" class="form-control"  placeholder="请输入菜单地址">
                            </div>
                            <div class="form-group">
                                <label>菜单状态</label>
                                <select class="form-control" >
                                    <option value="">全部</option>
                                    <option value="0">启用</option>
                                    <option value="1">禁用</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>菜单类型</label>
                                <select class="form-control" >
                                    <option value="">全部</option>
                                    <option value="0">目录</option>
                                    <option value="1">菜单</option>
                                </select>
                            </div>

                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 查询</button>

                        <a href="#" class="btn btn-success btn-input">
                            <span class="glyphicon glyphicon-plus"></span> 添加
                        </a>
                    </form>

                </div>
                <div class="box-body table-responsive ">
                <table class="table table-hover table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>编号</th>
<#--                        <th>菜单图标</th>-->
                        <th>菜单类型</th>
                        <th>菜单名称</th>
                        <th>上级菜单</th>
                        <th>菜单URL</th>
                        <th>权限表达式</th>
                        <th>排序</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
<#--                        <td> <i class="fa fa-dashboard"></i></td>-->
                        <td>目录</td>
                        <td>门店管理</td>
                        <td>无</td>
                        <td>无</td>
                        <td>无</td>
                        <td>1</td>
                        <td>启用</td>
                        <td>
                            <a href="#" class="btn btn-info btn-xs btn-input" >
                                <span class="glyphicon glyphicon-pencil"></span> 编辑
                            </a>
                            <a class="btn btn-xs btn-primary btn-status">
                                <span class="glyphicon glyphicon-phone-alt"></span> 启用</a>
                            <a class="btn btn-xs btn-danger btn-status" >
                                <span class="glyphicon glyphicon-remove"></span> 禁用</a>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
<#--                        <td> <i class="fa fa-files-o"></i></td>-->
                        <td>菜单</td>
                        <td>业务预约</td>
                        <td>门店管理</td>
                        <td>/appointment/list</td>
                        <td>appointment:list</td>
                        <td>1</td>
                        <td>启用</td>
                        <td>
                            <a href="#" class="btn btn-info btn-xs btn-input" >
                                <span class="glyphicon glyphicon-pencil"></span> 编辑
                            </a>
                            <a class="btn btn-xs btn-primary btn-status">
                                <span class="glyphicon glyphicon-phone-alt"></span> 启用</a>
                            <a class="btn btn-xs btn-danger btn-status" >
                                <span class="glyphicon glyphicon-remove"></span> 禁用</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
                    <!--分页-->
                    <#include "/common/page.ftl">
                </div>

            </div>
        </section>
    </div>
    <#include "/common/footer.ftl">
</div>

<script>

    $(".btn-input").click(function () {
        $("#editModal").modal('show')
    })
</script>
<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">新增/编辑</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="editForm" action="/aaaa" method="post" >
                <div class="modal-body">
                    <input type="hidden" >
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">菜单类型：</label>
                        <div class="col-sm-7">
                            <label>
                                <input type="radio" name="a"  value="0" checked>
                                目录
                            </label>
                            <label>
                                <input type="radio" name="a"  value="1">
                                菜单
                            </label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">菜单名称：</label>
                        <div class="col-sm-7">
                            <input type="text"  class="form-control " placeholder="请输入菜单名称"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">上级菜单：</label>
                        <div class="col-sm-7">
                            <select class="form-control" >
                                <option value="1">门店管理</option>
                                <option value="2">系统管理</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">菜单URL：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control"
                                   placeholder="请输入菜单URL">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">菜单序号：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control"
                                   placeholder="请输入菜单序号">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">菜单图标：</label>
                        <div class="col-sm-7" style="color:lightslategrey">
                                <label>
                                    <input type="radio" name="b"  value="fa fa-dashboard" checked>
                                    <i class="fa fa-dashboard"></i>
                                </label>
                                <label>
                                    <input type="radio" name="b"  value="fa fa-pie-chart">
                                    <i class="fa fa-pie-chart"></i>
                                </label>
                                <label>
                                    <input type="radio" name="b" value="fa fa-joomla" >
                                    <i class="fa fa-joomla"></i>
                                </label>
                                <label>
                                    <input type="radio" name="b"  value="fa fa-files-o" checked>
                                    <i class="fa fa-files-o"></i>
                                </label>
                                <label>
                                    <input type="radio" name="b"  value="fa fa-edit">
                                    <i class="fa fa-edit"></i>
                                </label>
                                <label>
                                    <input type="radio" name="b" value="fa fa-laptop" >
                                    <i class="fa fa-laptop"></i>
                                </label>
                                <label>
                                    <input type="radio" name="b" value="fa fa-laptop" >
                                    <i class="fa fa-circle-o"></i>
                                </label>
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">权限表达式：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control"
                                   placeholder="请输入权限表达式">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary btn-submit">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>


</body>
</html>
