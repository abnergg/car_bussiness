﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>预约单管理</title>
    <#include "/common/link.ftl">
    <script>
        $(function () {
            $('.input-date').datepicker({
                format: 'yyyy-mm-dd', //格式
                language: 'zh-CN', //中文
                autoclose: true,//选择后自动关闭
                //showMeridian:true, //是否显示上下午
                minView: 2,//精确到哪位
            });

            $('.btn-consume').click(function () {
                var id = $(this).data('id');
                Swal.fire({
                    title: '温馨提示',
                    text: '是否确认用户已经到店？',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: 'rgba(0,0,0,0.44)',
                    confirmButtonText: '是',
                    cancelButtonText: '否'
                }).then((result) => {
                    if (result.value) {
                        Swal.fire({
                            title: '温馨提示',
                            text: '客户是否进行消费？',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: 'rgba(0,0,0,0.44)',
                            confirmButtonText: '有消费',
                            cancelButtonText: '无消费'
                        }).then((result) => {
                            if (result.value) {
                                window.location.href = "/consumption/input1?status=65&aid=" + id;
                            } else {
                                window.location.href = "/appointment/updateStatus?status=66&id=" + id;
                            }
                        });
                    }
                });
            });
        });
    </script>
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <#include "/common/navbar.ftl">
    <!--菜单回显-->
    <#assign currentMenu="appointment"/>
    <#include "/common/menu.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>预约单管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <div style="margin: 20px 0px 0px 10px">
                    <form class="form-inline" id="searchForm" action="/appointment/list" method="post">
                        <input type="hidden" name="currentPage" id="currentPage" value="${qo.currentPage}">
                        <div class="form-group">
                            <label>预约单流水号</label>
                            <input type="text" class="form-control" placeholder="请输入预约单流水号" name="ano"
                                   value="${qo.ano}">
                        </div>

                        <div class="form-group">
                            <label>预约单状态</label>
                            <select class="form-control" id="status" name="status">
                                <option value="-1">全部</option>
                                <#list systemDictionaries1 as sys1>
                                    <option value="${sys1.id}">${sys1.name}</option>
                                </#list>
                            </select>
                            <script>
                                $('#status').val(${qo.status});
                            </script>
                        </div>
                        <div class="form-group">
                            <label>门店查询</label>
                            <select class="form-control" id="businessId" name="businessId">
                                <option value="-1">请选择门店</option>
                                <#list businesses as business>
                                    <option value="${business.id}">${business.name}</option>
                                </#list>
                            </select>
                            <script>
                                $('#businessId').val(${qo.businessId});
                            </script>
                        </div>
                        <div class="form-group">
                            <label>客户名称</label>
                            <input type="text" class="form-control" placeholder="请输入客户名称" name="contactName"
                                   value="${qo.contactName}">
                        </div>

                        <div class="form-group">
                            <label>客户手机号</label>
                            <input type="text" class="form-control" placeholder="请输入客户手机号" name="contactTel"
                                   value="${qo.contactTel}">
                        </div>

                        <br/>
                        <br/>

                        <div class="form-group">
                            <label>预约时间查询：</label>
                            <input placeholder="请输入开始时间" type="text" class="form-control input-date" name="startDate"
                                   value="${(qo.startDate?string("yyyy-MM-dd"))!}"/> -
                            <input placeholder="请输入结束时间" type="text" class="form-control input-date" name="endDate"
                                   value="${(qo.endDate?string("yyyy-MM-dd"))!}"/>
                        </div>

                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>
                            查询
                        </button>

                        <a href="#" class="btn btn-success btn-input">
                            <span class="glyphicon glyphicon-plus"></span> 添加
                        </a>
                    </form>

                </div>
                <div class="box-body table-responsive ">
                    <table class="table table-hover table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>序号</th>
                            <th>流水号</th>
                            <th>业务分类</th>
                            <th>预约说明</th>
                            <th>预约时间</th>
                            <th>客户名称</th>
                            <th>联系方式</th>
                            <th>预约门店</th>
                            <th>状态</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <#list pageInfo.list as a>
                            <tr>
                                <td>${a_index + 1}</td>
                                <td>${a.ano!}</td>
                                <td>${a.systemDictionary.name!}</td>
                                <td>${a.info!}</td>
                                <td>${(a.appointmentTime?string("yyyy-MM-dd"))!}</td>
                                <td>${a.contactName!}</td>
                                <td>${a.contactTel!}</td>
                                <td>${a.business.name!}</td>
                                <td>${a.statusDisplay()!}</td>
                                <td>
                                    <a href="#" class="btn btn-info btn-xs btn-input date-json" data-json='${a.toJSON()}'>
                                        <span class="glyphicon glyphicon-pencil"></span> 编辑
                                    </a>
                                    <a class="btn btn-xs btn-primary btn-status">
                                        <span class="glyphicon glyphicon-phone-alt"></span> 确认预约</a>
                                    <a class="btn btn-xs btn-danger btn-status">
                                        <span class="glyphicon glyphicon-remove"></span> 取消预约</a>
                                    <a href="#" class="btn btn-success btn-xs btn-consume" data-id="${a.id}">
                                        <span class="glyphicon glyphicon-shopping-cart"></span> 确认到店
                                    </a>
                                </td>
                            </tr>
                        </#list>
                        </tbody>
                    </table>
                    <!--分页-->
                    <#include "/common/page.ftl">
                </div>

            </div>
        </section>
    </div>
    <#include "/common/footer.ftl">
</div>


<#-- 文件上传模态框 -->
<!--模态框-->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">新增/编辑</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="editForm" action="/appointment/saveOrUpdate" method="post">
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">预约门店：</label>
                        <div class="col-sm-7">
                            <select class="form-control" name="business.id" id="businessId">
                                <option value="-1">请选择预约门店</option>
                                <#list businesses as business>
                                    <option value="${business.id}">${business.name}</option>
                                </#list>
                            </select>

                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">预约时间：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control input-date" placeholder="请输入预约时间"
                                   name="appointmentTime" id="appointmentTime"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">业务分类：</label>
                        <div class="col-sm-7">
                            <select class="form-control category" id="businessCategory">
                                <option value="-1">请选择预约业务</option>
                                <#list systemDictionaries as sys>
                                    <option value="${sys.id}">${sys.name}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label input-date">预约业务：</label>
                        <div class="col-sm-7">
                            <select class="form-control business" name="systemDictionary.id" id="businessModal">
                                <option value="-1">请选择预约业务</option>

                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">联系人：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control"
                                   placeholder="请输入联系人" name="contactName" id="contactName">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">联系电话：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control"
                                   placeholder="请输入联系电话" name="contactTel" id="contactTel">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">预约说明：</label>
                        <div class="col-sm-7">
                            <textarea type="text" class="form-control"
                                      placeholder="请输入预约说明" name="info" id="info"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary btn-submit">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.btn-input').click(function () {

            $('#editModal').clearForm(true);
            $("#editModal").modal('show');
            $("#businessCategory").change(function () {
                var value = $(this).val();
                var html = "<option value='-1'>请选择预约业务</option>";
                if (value == -1) {
                    $("#businessModal").html(html);
                    return;
                }
                $.get("/appointment/queryByParentId?parentId=" + value, function (data) {

                    $.each(data, function (index, item) {
                        html += "<option value='" + item.id + "'>" + item.name + "</option>"
                        $("#businessModal").html(html);
                    })

                })
            });
            var json = $(this).data('json');
            if (json){
                console.log(json);
                $('#id').val(json.id);
                $('#editForm select[name="business.id"]').val(json.businessId);
                $('#appointmentTime').val(json.appointmentTime);
                $('#businessCategory').val(json.parentId);

                $('#businessModal').val(json.categoryId);
                $('#contactName').val(json.contactName);
                $('#contactTel').val(json.contactTel);
                $('#info').val(json.info);

                var html = "<option value='-1'>请选择预约业务</option>";
                $.get("/appointment/queryByParentId?parentId=" + json.parentId, function (data) {

                    $.each(data, function (index, item) {
                        html += "<option value='" + item.id + "'>" + item.name + "</option>"
                        $("#businessModal").html(html);
                    });
                    $('#businessModal').val(json.categoryId);
                })
            }


        });
    });

</script>

</body>
</html>
