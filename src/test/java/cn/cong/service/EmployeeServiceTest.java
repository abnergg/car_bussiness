package cn.cong.service;

import cn.cong.domain.Employee;
import cn.cong.mapper.EmployeeMapper;
import cn.cong.qo.EmployeeQueryObject;
import cn.cong.qo.PageResult;
import com.alibaba.druid.pool.DruidDataSource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class EmployeeServiceTest {

    @Autowired
    private IEmployeeService employeeService;

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private DruidDataSource druidDataSource;

    @Test
    public void selectAll() {
/*        EmployeeQueryObject queryObject = new EmployeeQueryObject();
//        queryObject.setKeyword("s");
        PageResult<Employee> query = employeeService.query(queryObject);
        System.out.println("query = " + query.getData());*/
        System.out.println(employeeMapper.selectByPrimaryKey(1l));
    }

    @Test
    public void test(){
//        List<Employee> employees = employeeMapper.selectForList(new EmployeeQueryObject());
//        employees.forEach(System.out::println);
        System.out.println(druidDataSource);
    }

}