package cn.cong.mapper;

import cn.cong.domain.Permission;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class PermissionMapperTest {

    @Autowired
    private PermissionMapper permissionMapper;

    @Test
    public void batchInsert() {

        Set<Permission> permissions = new LinkedHashSet<>();
        Permission permission = new Permission();
        permission.setName("sss");
        permission.setExpression("sss");
        permissions.add(permission);

        permissionMapper.batchInsert(permissions);
    }
}